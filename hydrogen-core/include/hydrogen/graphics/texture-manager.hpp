#pragma once

#include <vector>
#include <memory>

#include "hydrogen/graphics/vk-utils.hpp"

namespace hgn::gfx::vk {
class VkContext;
}

namespace hgn::gfx {

class DescriptorBuilder;

class TextureSet {
public:

    friend class TextureManager;
protected:
    VkDescriptorSetLayout descriptor_layout_;
    std::vector<VkDescriptorSet> descriptor_sets_;

};

struct TextureUpdate {
    uint32_t texture_index;
    VkSampler sampler;
    VkImageView image_view;
    TextureSet* texture_set;
};

class TextureManager {
public:
    void init(vk::VkContext* context);

    void sync_texture_sets(uint32_t frame_index);
    TextureSet create_texture_set();

    void queue_texture_update(TextureUpdate update);

private:
    vk::VkContext* context_;

    // texture_update[buffered frame index][update index]
    std::vector<std::vector<TextureUpdate>> texture_updates_;
};

}