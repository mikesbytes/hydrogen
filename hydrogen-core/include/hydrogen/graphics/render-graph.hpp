#pragma once 

#include "hydrogen/graphics/vk-mesh.hpp"
#include "hydrogen/graphics/vk/material-data.hpp"

#include <glm/glm.hpp>

namespace hgn::gfx {

struct RenderItem {
    hgn::gfx::vk::Mesh mesh;
    glm::mat4 model_mat;
    uint8_t frame_update_counter;
    hgn::gfx::vk::MaterialData material_data;
    bool free;
};

typedef std::vector<RenderItem*> RenderList;

class RenderGraph {
public:
    RenderItem* add_render_object(RenderItem render_item);

    RenderList get_render_list();

private:
    std::vector<RenderItem> render_items_;
};

}