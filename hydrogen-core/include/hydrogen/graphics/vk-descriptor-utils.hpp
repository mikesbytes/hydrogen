#pragma once

#include <unordered_map>
#include <optional>

#include "hydrogen/graphics/vk-utils.hpp"

namespace hgn::gfx {

class DescriptorAllocator {
public:
    struct PoolSizes {
        std::vector<std::pair<VkDescriptorType, float>> sizes = {
            {VK_DESCRIPTOR_TYPE_SAMPLER, 0.5f},
            {VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 4.f },
            {VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, 4.f },
            {VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1.f },
            {VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER, 1.f },
            {VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER, 1.f },
            {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 2.f },
            {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 2.f },
            {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, 1.f },
            {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC, 1.f },
            {VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, 0.5f }
        };
    };

    DescriptorAllocator();

    void reset_pools();
    VkDescriptorSet allocate(VkDescriptorSetLayout layout, std::optional<uint32_t> binding_count = std::nullopt);

    void init(VkDevice device);
    void cleanup();

    VkDevice device_;
private:
    VkDescriptorPool grab_pool();

    PoolSizes descriptor_sizes_;
    VkDescriptorPool current_pool_;
    std::vector<VkDescriptorPool> used_pools_;
    std::vector<VkDescriptorPool> free_pools_;

};

class DescriptorLayoutCache {
public:
    struct DescriptorLayoutInfo {
        std::vector<VkDescriptorSetLayoutBinding> bindings;
        
        bool operator==(const DescriptorLayoutInfo& other) const;

        size_t hash() const;
    };

    void init(VkDevice device);
    void cleanup();

    VkDescriptorSetLayout create_descriptor_layout(VkDescriptorSetLayoutCreateInfo* info);

private:
    struct DescriptorLayoutHash {
        std::size_t operator()(const DescriptorLayoutInfo& k) const {
            return k.hash();
        }
    };

    std::unordered_map<DescriptorLayoutInfo, VkDescriptorSetLayout, DescriptorLayoutHash> layout_cache_;
    VkDevice device_;
};

class DescriptorBuilder {
public:
    static DescriptorBuilder begin(
        DescriptorLayoutCache* cache,
        DescriptorAllocator* allocator
    );

    DescriptorBuilder& bind_buffer(
        uint32_t binding,
        VkDescriptorBufferInfo* buffer_info,
        VkDescriptorType type,
        VkShaderStageFlags flags
    );

    DescriptorBuilder& bind_image(
        uint32_t binding,
        VkDescriptorImageInfo* image_info,
        VkDescriptorType type,
        VkShaderStageFlags flags
    );

    DescriptorBuilder& bind_bindless(
        uint32_t binding,
        VkDescriptorType type,
        VkShaderStageFlags flags,
        uint32_t max_resource_count
    );

    std::pair<VkDescriptorSetLayout, VkDescriptorSet> build();

private:
    std::vector<VkWriteDescriptorSet> writes_;
    std::vector<VkDescriptorSetLayoutBinding> bindings_;

    DescriptorLayoutCache* cache_;
    DescriptorAllocator* allocator_;
    bool has_bindless_bind_;
    uint32_t bindless_max_resource_count_;
};

VkDescriptorPool createPool(
    VkDevice device,
    const DescriptorAllocator::PoolSizes& pool_sizes,
    int count,
    VkDescriptorPoolCreateFlags flags
);


}