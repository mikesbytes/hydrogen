#pragma once

#include <vulkan/vulkan.hpp>
#define VMA_STATIC_VULKAN_FUNCTIONS 0
#define VMA_DYNAMIC_VULKAN_FUNCTIONS 1
#include <vk_mem_alloc.h>
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>

#include "hydrogen/graphics/vk-utils.hpp"
#include "hydrogen/graphics/vk/buffer.hpp"

namespace hgn::gfx::vk {

class VkContext;

// forward declarations
struct VertexInputDescription {
    std::vector<VkVertexInputBindingDescription> bindings;
    std::vector<VkVertexInputAttributeDescription> attributes;

    VkPipelineVertexInputStateCreateFlags flags = 0;
};

struct Vertex {
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec2 uv;

    static VertexInputDescription get_vertex_description();
};

class Mesh {
public:
    Mesh(const Mesh& rhs) = delete;

    Mesh();
    Mesh(VkContext* context, vk::Buffer&& vertex_buffer, vk::Buffer&& index_buffer,
        size_t vertex_count, size_t index_count);
    Mesh(Mesh&& rhs);
    ~Mesh();

    Mesh& operator=(const Mesh& rhs) = delete;
    Mesh& operator=(Mesh&& rhs);

    hgn::gfx::vk::Buffer vertex_buffer;
    hgn::gfx::vk::Buffer index_buffer;

    //AllocatedBuffer vertex_buffer;
    //AllocatedBuffer index_buffer;

    size_t vertex_count() const;
    size_t index_count() const;

    void destroy_if_initialized();
private:

    bool initialized;
    VkContext* context_;

    size_t vertex_count_;
    size_t index_count_;

};

Mesh loadMesh(vk::VkContext* ctx, const std::vector<Vertex>& verts, const std::vector<uint32_t>& indices);

Mesh loadMeshFromOBJ(vk::VkContext* ctx, const std::string& file_name);


}