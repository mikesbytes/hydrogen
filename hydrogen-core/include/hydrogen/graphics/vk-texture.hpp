#pragma once

#include <vulkan/vulkan.hpp>
#define VMA_STATIC_VULKAN_FUNCTIONS 0
#define VMA_DYNAMIC_VULKAN_FUNCTIONS 1
#include <vk_mem_alloc.h>

#include "hydrogen/graphics/vk-utils.hpp"

namespace hgn::gfx::vk {
class VkContext;
}

namespace hgn::gfx {

class Texture {
public:
    Texture(const Texture& rhs) = delete;
    Texture(Texture&& rhs);
    Texture();
    Texture(
        vk::VkContext* context, AllocatedImage image, VkImageView image_view,
        size_t width, size_t height
    );
    ~Texture();

    Texture& operator=(const Texture& rhs) = delete;
    Texture& operator=(Texture&& rhs);

    bool loaded();

    AllocatedImage image;
    VkImageView image_view;
    size_t width;
    size_t height;
private:
    void destroy_if_loaded();

    vk::VkContext* context_;
    bool loaded_;
};


Texture loadTextureFromFile(
    vk::VkContext* ctx,
    const std::string& file_name
);

}