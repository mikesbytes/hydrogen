#pragma once

#include <vulkan/vulkan.hpp>
#define VMA_STATIC_VULKAN_FUNCTIONS 0
#define VMA_DYNAMIC_VULKAN_FUNCTIONS 1
#include <vk_mem_alloc.h>
#include <fmt/format.h>

#define VK_CALL(VK_FN) { \
    VkResult vk_call_res = VK_FN; \
    if (vk_call_res != VK_SUCCESS) { \
        throw std::runtime_error(fmt::format("Call to {} failed; error code {}", #VK_FN, vk_call_res)); \
    } \
} 

namespace hgn::gfx {

struct AllocatedBuffer {
    VkBuffer buffer;
    VmaAllocation allocation;
};

struct AllocatedImage {
    VkImage image;
    VmaAllocation allocation;
};

}