#include <string>
#include <optional>
#include <vector>

namespace hgn::gfx {

struct ModelData {
    std::vector<std::vector<uint8_t>> buffers;

};

std::optional<ModelData> looadGLTFModel(const std::string& model_path);

}