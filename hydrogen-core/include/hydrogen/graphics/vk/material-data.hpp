#pragma once

#include <vector>
#include <memory>

namespace hgn::gfx::vk {

class Material;
class DescriptorSet;

struct MaterialData {
    //std::shared_ptr<Material> linked_material_;
    std::vector<std::shared_ptr<DescriptorSet>> descriptor_sets_;
};

}