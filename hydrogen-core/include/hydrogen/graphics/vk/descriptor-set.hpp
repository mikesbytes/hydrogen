#pragma once

#include <memory>

#include "hydrogen/graphics/vk-utils.hpp"

namespace hgn::gfx::vk {

class VkContext;

class DescriptorSetLayout;

class DescriptorSet {
public:
    DescriptorSet(VkContext* context, std::shared_ptr<DescriptorSetLayout> descriptor_set_layout, VkDescriptorSet descriptor_set);
    DescriptorSet(const DescriptorSet&) = delete;
    DescriptorSet(DescriptorSet&& rhs);
    ~DescriptorSet();

    DescriptorSet& operator=(const DescriptorSet&) = delete;
    DescriptorSet& operator=(DescriptorSet&& rhs);

private:
    void destroy_if_initialized();

    VkContext* context_;
    std::shared_ptr<DescriptorSetLayout> descriptor_set_layout_;
    VkDescriptorSet descriptor_set_;
};

}