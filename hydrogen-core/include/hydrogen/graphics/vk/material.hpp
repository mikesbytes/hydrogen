#pragma once

#include <memory>

namespace hgn::gfx::vk {

// forward declarations

class Pipeline;

// end forward declarations

class Material {
public:
    Material(std::shared_ptr<Pipeline> pipeline);
private:
    std::shared_ptr<Pipeline> pipeline_;

};

}