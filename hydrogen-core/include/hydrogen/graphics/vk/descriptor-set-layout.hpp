#pragma once

#include "hydrogen/graphics/vk-utils.hpp"

namespace hgn::gfx::vk {
class VkContext;

class DescriptorSetLayout {
public:
    DescriptorSetLayout(VkContext* context, VkDescriptorSetLayout layout);
    DescriptorSetLayout(const DescriptorSetLayout&) = delete;
    DescriptorSetLayout(DescriptorSetLayout&& rhs);
    ~DescriptorSetLayout();

    DescriptorSetLayout& operator=(const DescriptorSetLayout&) = delete;
    DescriptorSetLayout& operator=(DescriptorSetLayout&& rhs);

    VkDescriptorSetLayout* get_raw_descriptor_set_layout();

private:
    void destroy_if_initialized();

    VkDescriptorSetLayout descriptor_set_layout_;
    VkContext* context_;
};

}