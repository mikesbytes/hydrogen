#pragma once

#include "hydrogen/graphics/vk/buffer.hpp"

namespace hgn::gfx::vk {

class VkContext;

void buffer_copy(VkContext* context,
                 Buffer* src, size_t src_offset,
                 Buffer* dst, size_t dst_offset,
                 size_t size);

}