#pragma once

#include "hydrogen/graphics/vk-utils.hpp"

namespace hgn::gfx::vk {
    //create_depth_resources();

class VkContext;

class Pipeline {
public:
    Pipeline(VkContext* context, VkPipeline pipeline_, VkPipelineLayout pipeline_layout);
    Pipeline(const Pipeline&) = delete;
    Pipeline(Pipeline&& rhs);
    ~Pipeline();

    Pipeline& operator=(const Pipeline&) = delete;
    Pipeline& operator=(Pipeline&& rhs);

    VkPipeline* get_pipeline();

private:
    void destroy_if_initialized();

    bool initialized_;
    VkPipeline pipeline_;
    VkPipelineLayout pipeline_layout_;
    VkContext* context_;
};

}