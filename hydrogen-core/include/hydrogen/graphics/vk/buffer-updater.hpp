#pragma once

#include <stddef.h>
#include "hydrogen/graphics/vk/buffer.hpp"

namespace hgn::gfx::vk {

class VkContext;

class BufferUpdater {
public:
    // defaults to 1M update buffer size
    BufferUpdater(VkContext* context, size_t max_frames, size_t update_buffer_size = 1048576);

    void submit_update(
        void* src, size_t size,
        std::weak_ptr<Buffer> dst, size_t dst_offset,
        size_t frame
    );

    void process_updates();

private:
    VkContext* context_;
    size_t max_frames_;
    std::vector<Buffer> partial_update_buffers_;
    std::vector<size_t> partial_update_indexes_;

    struct PartialBufferUpdate {
        std::weak_ptr<Buffer> dst_buffer;
        size_t src_offset;
        size_t dst_offset;
        size_t size;
    };

    // [frame][update index]
    std::vector<std::vector<PartialBufferUpdate>> partial_updates_;
};

}