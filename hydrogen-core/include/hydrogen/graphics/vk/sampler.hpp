#pragma once

#include "hydrogen/graphics/vk-utils.hpp"

namespace hgn::gfx::vk {

class VkContext;

class Sampler {
public:
    Sampler();
    Sampler(const Sampler& rhs) = delete;
    Sampler(Sampler&& rhs);

    ~Sampler();

    Sampler& operator=(const Sampler& rhs) = delete;
    Sampler& operator=(Sampler&& rhs);

    VkSampler* sampler();

    static Sampler create_default_sampler(VkContext* context);

private:
    VkContext* context_;
    VkSampler sampler_;
};

}