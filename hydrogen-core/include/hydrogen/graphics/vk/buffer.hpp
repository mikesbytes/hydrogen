#pragma once

#include "hydrogen/graphics/vk-utils.hpp"
#include "stddef.h"

namespace hgn::gfx::vk {

class VkContext;

class Buffer {
public:
    Buffer();
    Buffer(VkContext* context, VkBuffer buffer, VmaAllocation allocation, size_t size, void* mapping = nullptr);
    Buffer(const Buffer& rhs) = delete;
    Buffer(Buffer&& rhs);

    ~Buffer();

    Buffer& operator=(const Buffer& rhs) = delete;
    Buffer& operator=(Buffer&& rhs);

    VkBuffer* get_buffer();
    void* get_mapping();

    void destroy_if_initialized();

    void* map();

    void unmap();

    // calls vmaFlushAllocation. Size 0 will flush whole buffer
    void flush(size_t offset = 0, size_t size = 0);
    
    // calls vmaInvalidateAllocation. Size 0 will invalidate whole buffer
    void invalidate(size_t offset = 0, size_t size = 0);

    static Buffer make_buffer(VkContext* context, size_t size, VkBufferUsageFlags usage);
    static Buffer make_mapped_buffer(VkContext* context, size_t size, VkBufferUsageFlags usage);

private:
    VkContext* context_;
    VkBuffer buffer_;
    VmaAllocation allocation_;
    void* mapping_;
    size_t size_;

};

}