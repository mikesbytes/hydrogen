#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>
#include <vulkan/vulkan.hpp>
#include <optional>
#define VMA_STATIC_VULKAN_FUNCTIONS 0
#define VMA_DYNAMIC_VULKAN_FUNCTIONS 1
#include <vk_mem_alloc.h>
#include <functional>

#include "hydrogen/graphics/vk-mesh.hpp"
#include "hydrogen/graphics/vk-texture.hpp"
#include "hydrogen/graphics/vk-descriptor-utils.hpp"
#include "hydrogen/graphics/render-graph.hpp"
#include "hydrogen/graphics/objects/render-obj.hpp"

namespace hgn::gfx::vk {

class VkContext {
public:
    void init_vulkan();

    void loop();

    // public because c++ is stupid
    struct QueueFamilyIndices_ {
        std::optional<uint32_t> graphics_family;
        std::optional<uint32_t> present_family;

        bool is_complete();
    };

    struct SwapChainSupportDetails {
        VkSurfaceCapabilitiesKHR capabilities;
        std::vector<VkSurfaceFormatKHR> formats;
        std::vector<VkPresentModeKHR> present_modes;
    };

    VkDevice* get_device();
    VkPhysicalDevice* get_physical_device();
    VmaAllocator* get_allocator();
    RenderGraph* get_render_graph();

    void copy_buffer(VkBuffer src_buffer, VkBuffer dst_buffer, VkDeviceSize size);

    void copy_buffer_to_image(
        VkBuffer buffer, VkImage image, uint32_t width, uint32_t height
    );

    void transition_image_layout(VkImage image, VkFormat format,
        VkImageLayout old_layout, VkImageLayout new_layout);


    void process_events(std::function<void(SDL_Event)> event_callback);

    void start_gui_frame();

    void end_gui_frame();

    void draw_frame();

    void sync_frame_data();

    void present_frame();

    void immediate_submit(std::function<void(VkCommandBuffer)>&& fn);

    void frame_submit(std::function<void(VkCommandBuffer)>&& fn);
    
    void cleanup();

    uint32_t get_current_frame();
private:

    // private fns
    void init_window();

    void create_instance();

    bool check_validation_layer_support();

    std::vector<const char*> get_required_extensions();

    static VKAPI_ATTR VkBool32 VKAPI_CALL debug_callback(
        VkDebugUtilsMessageSeverityFlagBitsEXT message_severity,
        VkDebugUtilsMessageTypeFlagsEXT message_type,
        const VkDebugUtilsMessengerCallbackDataEXT* p_callback_data,
        void* p_user_data);

    void setup_debug_messenger();

    void pick_physical_device();

    bool is_device_suitable(VkPhysicalDevice device);

    void create_logical_device();

    void create_surface();

    QueueFamilyIndices_ find_queue_families(VkPhysicalDevice device);

    bool check_device_extension_support(VkPhysicalDevice device);

    SwapChainSupportDetails query_swap_chain_support(VkPhysicalDevice device);

    VkSurfaceFormatKHR choose_swap_surface_format(
        const std::vector<VkSurfaceFormatKHR>& available_formats
    );

    VkPresentModeKHR choose_swap_present_mode(
        const std::vector<VkPresentModeKHR>& available_modes
    );

    VkExtent2D choose_swap_extent(const VkSurfaceCapabilitiesKHR& capabilities);

    void create_swap_chain();

    void create_image_views();

    void create_graphics_pipeline();

    VkShaderModule create_shader_module(const std::vector<char>& data);

    void create_render_pass();

    void create_frame_buffers();

    void create_command_pool();

    void create_command_buffers();

    void record_command_buffer(VkCommandBuffer buffer, uint32_t image_index);

    void create_sync_objects();
    
    void cleanup_swap_chain();
    
    void recreate_swap_chain();

    void create_vertex_buffer();

    void create_index_buffer();

    uint32_t find_memory_type(uint32_t type_filter, VkMemoryPropertyFlags props);

    void create_buffer(
        VkDeviceSize size, VkBufferUsageFlags usage, std::optional<VmaAllocationCreateFlagBits> properties,
        VkBuffer* buffer, VmaAllocation* buffer_memory
    );

    void create_descriptor_set_layout();

    void create_uniform_buffers();

    void update_uniform_buffer(uint32_t current_image);

    void create_texture_image();

    void create_image(
        uint32_t width, uint32_t height,
        VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, VkMemoryPropertyFlags properties,
        VkImage* image, VkDeviceMemory* image_memory
    );

    VkCommandBuffer begin_single_time_commands();

    void end_single_time_commands(VkCommandBuffer buffer);

    VkImageView create_image_view(VkImage image, VkFormat format, VkImageAspectFlags flags = VK_IMAGE_ASPECT_COLOR_BIT);

    void create_texture_sampler();

    void create_depth_resources();

    VkFormat find_supported_format(
        const std::vector<VkFormat>& candidates,
        VkImageTiling tiling,
        VkFormatFeatureFlags features);

    VkFormat find_depth_format();

    bool has_stencil_component(VkFormat format);

    void create_allocator();

    void init_imgui();
    

    // private fields
    SDL_Window *window_;
    VkInstance instance_;
    VkDebugUtilsMessengerEXT debug_messenger_;
    VkPhysicalDevice physical_device_;
    VkDevice device_;
    VkQueue graphics_queue_;
    VkQueue present_queue_;
    VkSurfaceKHR surface_;
    VkSwapchainKHR swap_chain_;
    std::vector<VkImage> swap_chain_images_;
    VkFormat swap_chain_image_format_;
    VkExtent2D extent_;
    std::vector<VkImageView> swap_chain_image_views_;
    VkRenderPass render_pass_;
    VkPipelineLayout pipeline_layout_;
    VkPipeline pipeline_;
    std::vector<VkFramebuffer> swap_chain_framebuffers_;
    VkCommandPool command_pool_;
    std::vector<VkCommandBuffer> command_buffers_;
    std::vector<VkSemaphore> image_available_sems_;
    std::vector<VkSemaphore> render_finished_sems_;
    std::vector<VkFence> in_flight_fences_;
    uint32_t current_frame_;
    bool frame_buffer_resized_;
    Mesh mesh_;
    VkBuffer index_buffer_;
    VmaAllocation index_buffer_memory_;
    std::vector<VkBuffer> uniform_buffers_;
    std::vector<VmaAllocation> uniform_buffer_memories_;
    std::vector<gfx::AllocatedBuffer> scene_obj_buffers_;
    std::vector<gfx::AllocatedBuffer> scene_light_buffers_;
    std::vector<void*> uniform_buffer_mappings_;
    std::vector<void*> scene_obj_buffer_mappings_;
    std::vector<void*> scene_light_buffer_mappings_;
    VkDescriptorPool descriptor_pool_;
    VkDescriptorSetLayout descriptor_set_layout_;
    VkDescriptorSetLayout scene_data_descriptor_set_layout_;
    gfx::DescriptorAllocator descriptor_allocator_;
    gfx::DescriptorLayoutCache descriptor_layout_cache_;
    std::vector<VkDescriptorSet> descriptor_sets_;
    std::vector<VkDescriptorSet> scene_data_descriptor_sets_;
    Texture texture_;
    VkSampler texture_sampler_;
    VkImage depth_image_;
    VkDeviceMemory depth_image_memory_;
    VkImageView depth_image_view_;
    VmaAllocator allocator_;
    gfx::RenderGraph render_graph_;
    obj::RenderObject render_obj_;
    uint32_t swap_image_index_;
};

}