#pragma once

#include "hydrogen/graphics/vk-utils.hpp"

namespace hgn::gfx {

class PipelineBuilder {
public:
    std::vector<VkPipelineShaderStageCreateInfo> shader_stages;
    VkPipelineVertexInputStateCreateInfo vertex_info;
    VkPipelineInputAssemblyStateCreateInfo input_assembly;
    VkViewport viewport;
    VkRect2D scissor;
    VkPipelineRasterizationStateCreateInfo rasterizer;
    VkPipelineColorBlendAttachmentState color_blend_attachment;
    VkPipelineDepthStencilStateCreateInfo depth_stencil;
    VkPipelineMultisampleStateCreateInfo multisampling;
    VkPipelineLayoutCreateInfo pipeline_layout_info;
    VkPipelineLayout pipeline_layout;

    static VkPipelineShaderStageCreateInfo gen_shader_stage_create_info(
        VkShaderStageFlagBits stage, VkShaderModule module
    );

    static VkPipelineVertexInputStateCreateInfo gen_vertex_create_info(
        const std::vector<VkVertexInputBindingDescription>& bindings,
        const std::vector<VkVertexInputAttributeDescription>& attributes
    );

    static VkPipelineInputAssemblyStateCreateInfo gen_input_assembly_create_info(
        VkPrimitiveTopology topology
    );

    static VkViewport gen_viewport(
        float x, float y,
        float w, float h,
        float min_depth, float max_depth
    );

    static VkRect2D gen_scissor(
        int offset_x, int offset_y,
        uint32_t w, uint32_t h
    );

    static VkPipelineRasterizationStateCreateInfo gen_rasterizer_create_info(
        VkPolygonMode polygon_mode
    );

    static VkPipelineMultisampleStateCreateInfo gen_multisampling_create_info();

    static VkPipelineColorBlendAttachmentState gen_color_blend_attachment();

    static VkPipelineDepthStencilStateCreateInfo gen_depth_stencil_create_info();

    static VkPipelineLayoutCreateInfo gen_pipeline_layout_create_info(const std::vector<VkDescriptorSetLayout>& layouts);

    PipelineBuilder();

    PipelineBuilder& add_shader_stage(VkShaderStageFlagBits stage, VkShaderModule module);
    PipelineBuilder& create_vertex_info(
        const std::vector<VkVertexInputBindingDescription>& bindings,
        const std::vector<VkVertexInputAttributeDescription>& attributes
    );
    PipelineBuilder& create_input_assembly_info(VkPrimitiveTopology topology);
    PipelineBuilder& create_rasterizer(VkPolygonMode polygon_mode);
    PipelineBuilder& create_viewport(
        float x, float y,
        float w, float h,
        float min_depth, float max_depth
    );
    PipelineBuilder& create_scissor(
        int offset_x, int offset_y,
        uint32_t w, uint32_t h
    );
    PipelineBuilder& create_multisampling();
    PipelineBuilder& create_color_blend_attachment();
    PipelineBuilder& create_depth_stencil();
    PipelineBuilder& create_layout(const std::vector<VkDescriptorSetLayout>& layouts);

    std::pair<VkPipeline, VkPipelineLayout> build_pipeline(VkDevice device, VkRenderPass pass);
};

}