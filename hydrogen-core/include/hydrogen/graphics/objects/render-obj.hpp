#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

namespace hgn::gfx {

struct RenderItem;

}

namespace hgn::gfx::obj {

class RenderObject {
public:
    RenderObject();
    RenderObject(hgn::gfx::RenderItem* render_item);

    ~RenderObject();

    void set_render_item(hgn::gfx::RenderItem* render_item);

    void set_position(glm::vec3 position, bool no_rebuild = false);
    glm::vec3 get_position() const;

    void set_scale(glm::vec3 scale, bool no_rebuild = false);
    glm::vec3 get_scale();

    // pitch, roll, yaw
    void set_rotation(glm::quat rotation, bool no_rebuild = false);
    void set_rotation(glm::vec3 euler_rotation, bool no_rebuild = false);
    glm::quat get_rotation() const;

    void rebuild_model_mat();

private:

    glm::vec3 position_;
    glm::vec3 scale_;
    glm::quat rotation_;

    hgn::gfx::RenderItem* render_item_;
};

}