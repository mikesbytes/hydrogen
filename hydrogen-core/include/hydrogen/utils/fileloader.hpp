#include <vector>
#include <string>

namespace hgn::utils {

std::vector<char> read_binary_file(const std::string& file_name);

}