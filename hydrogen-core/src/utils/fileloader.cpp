#include "hydrogen/utils/fileloader.hpp"

#include <fstream>
#include <fmt/format.h>

namespace hgn::utils {

std::vector<char> read_binary_file(const std::string& file_name) {
    std::ifstream file(file_name, std::ios::ate | std::ios::binary);

    if (!file.is_open()) {
        throw std::runtime_error(fmt::format("Binary file read failed on {}", file_name));
    };

    size_t file_size = (size_t) file.tellg();
    std::vector<char> buffer(file_size);
    file.seekg(0);
    file.read(buffer.data(), file_size);

    return buffer;
}

}