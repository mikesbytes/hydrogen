#include "hydrogen/graphics/vk-pipeline-utils.hpp"

namespace hgn::gfx {

VkPipelineShaderStageCreateInfo PipelineBuilder::gen_shader_stage_create_info(
    VkShaderStageFlagBits stage, VkShaderModule module
) {
    VkPipelineShaderStageCreateInfo info = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .stage = stage,
        .module = module,
        .pName = "main",
        .pSpecializationInfo = nullptr,
    };

    return info;
}

VkPipelineVertexInputStateCreateInfo PipelineBuilder::gen_vertex_create_info(
    const std::vector<VkVertexInputBindingDescription>& bindings,
    const std::vector<VkVertexInputAttributeDescription>& attributes
) {
    VkPipelineVertexInputStateCreateInfo info = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
        .vertexBindingDescriptionCount = (uint32_t)bindings.size(),
        .pVertexBindingDescriptions = bindings.data(),
        .vertexAttributeDescriptionCount = (uint32_t)attributes.size(),
        .pVertexAttributeDescriptions = attributes.data()
    };

    return info;
}

VkPipelineInputAssemblyStateCreateInfo PipelineBuilder::gen_input_assembly_create_info(
    VkPrimitiveTopology topology
) {
    VkPipelineInputAssemblyStateCreateInfo info = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
        .topology = topology,
        .primitiveRestartEnable = VK_FALSE
    };

    return info;
}

VkViewport PipelineBuilder::gen_viewport(
    float x, float y,
    float w, float h,
    float min_depth, float max_depth
) {
    VkViewport viewport = {
        .x = x,
        .y = y,
        .width = w,
        .height = h,
        .minDepth = min_depth,
        .maxDepth = max_depth
    };
    return viewport;
}

VkRect2D PipelineBuilder::gen_scissor(
    int offset_x, int offset_y,
    uint32_t w, uint32_t h
) {
    VkRect2D scissor = {
        .offset = {offset_x, offset_y},
        .extent = {w, h}
    };
    return scissor;
}

VkPipelineRasterizationStateCreateInfo PipelineBuilder::gen_rasterizer_create_info(
    VkPolygonMode polygon_mode
) {
    VkPipelineRasterizationStateCreateInfo info = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .depthClampEnable = VK_FALSE,
        .polygonMode = polygon_mode,
        .cullMode = VK_CULL_MODE_BACK_BIT,
        .frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE,
        .depthBiasEnable = VK_FALSE,
        .depthBiasConstantFactor = 0.0f,
        .depthBiasClamp = 0.0f,
        .depthBiasSlopeFactor = 0.0f,
        .lineWidth = 1.0f
    };

    return info;
}

VkPipelineMultisampleStateCreateInfo PipelineBuilder::gen_multisampling_create_info() {
    VkPipelineMultisampleStateCreateInfo info = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
        .rasterizationSamples = VK_SAMPLE_COUNT_1_BIT,
        .sampleShadingEnable = VK_FALSE,
        .minSampleShading = 1.0f,
        .pSampleMask = nullptr,
        .alphaToCoverageEnable = VK_FALSE,
        .alphaToOneEnable = VK_FALSE
    };

    return info;
}

VkPipelineColorBlendAttachmentState PipelineBuilder::gen_color_blend_attachment() {
    VkPipelineColorBlendAttachmentState cba = {
        .blendEnable = VK_FALSE,
        .srcColorBlendFactor = VK_BLEND_FACTOR_ONE,
        .dstColorBlendFactor = VK_BLEND_FACTOR_ZERO,
        .colorBlendOp = VK_BLEND_OP_ADD,
        .srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
        .dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
        .alphaBlendOp = VK_BLEND_OP_ADD,
        .colorWriteMask =
            VK_COLOR_COMPONENT_R_BIT |
            VK_COLOR_COMPONENT_G_BIT |
            VK_COLOR_COMPONENT_B_BIT |
            VK_COLOR_COMPONENT_A_BIT
    };

    return cba;
}

VkPipelineDepthStencilStateCreateInfo PipelineBuilder::gen_depth_stencil_create_info() {
    VkPipelineDepthStencilStateCreateInfo create_info = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
        .depthTestEnable = VK_TRUE,
        .depthWriteEnable = VK_TRUE,
        .depthCompareOp = VK_COMPARE_OP_LESS,
        .depthBoundsTestEnable = VK_FALSE,
        .stencilTestEnable = VK_FALSE,
        .front = {},
        .back = {},
        .minDepthBounds = 0.0f,
        .maxDepthBounds = 1.0f
    };

    return create_info;
}

VkPipelineLayoutCreateInfo PipelineBuilder::gen_pipeline_layout_create_info(const std::vector<VkDescriptorSetLayout>& layouts) {
    VkPipelineLayoutCreateInfo create_info = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        .setLayoutCount = (uint32_t)layouts.size(),
        .pSetLayouts = layouts.data(),
        .pushConstantRangeCount = 0,
        .pPushConstantRanges = nullptr
    };

    return create_info;
}

PipelineBuilder::PipelineBuilder()
{}

PipelineBuilder& PipelineBuilder::add_shader_stage(VkShaderStageFlagBits stage, VkShaderModule module) {
    shader_stages.push_back(gen_shader_stage_create_info(stage, module));
    return *this;
}

PipelineBuilder& PipelineBuilder::create_vertex_info(
    const std::vector<VkVertexInputBindingDescription>& bindings,
    const std::vector<VkVertexInputAttributeDescription>& attributes
) {
    vertex_info = gen_vertex_create_info(bindings, attributes);
    return *this;
}

PipelineBuilder& PipelineBuilder::create_input_assembly_info(VkPrimitiveTopology topology) {
    input_assembly = gen_input_assembly_create_info(topology);
    return *this;
}

PipelineBuilder& PipelineBuilder::create_rasterizer(VkPolygonMode polygon_mode) {
    rasterizer = gen_rasterizer_create_info(polygon_mode);
    return *this;
}

PipelineBuilder& PipelineBuilder::create_viewport(
    float x, float y,
    float w, float h,
    float min_depth, float max_depth
) {
    viewport = gen_viewport(x,y,w,h,min_depth,max_depth);
    return *this;
}

PipelineBuilder& PipelineBuilder::create_scissor(
    int offset_x, int offset_y,
    uint32_t w, uint32_t h
) {
    scissor = gen_scissor(offset_x, offset_y, w, h);
    return *this;
}

PipelineBuilder& PipelineBuilder::create_multisampling() {
    multisampling = gen_multisampling_create_info();
    return *this;
}

PipelineBuilder& PipelineBuilder::create_color_blend_attachment() {
    color_blend_attachment = gen_color_blend_attachment();
    return *this;
}

PipelineBuilder& PipelineBuilder::create_depth_stencil() {
    depth_stencil = gen_depth_stencil_create_info();
    return *this;
}

PipelineBuilder& PipelineBuilder::create_layout(const std::vector<VkDescriptorSetLayout>& layouts) {
    pipeline_layout_info = gen_pipeline_layout_create_info(layouts);
    return *this;
}

std::pair<VkPipeline, VkPipelineLayout> PipelineBuilder::build_pipeline(VkDevice device, VkRenderPass pass) {
    VkPipeline pipeline;

    VK_CALL(vkCreatePipelineLayout(
        device,
        &pipeline_layout_info,
        nullptr,
        &pipeline_layout
    ));

    VkPipelineViewportStateCreateInfo viewport_state = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
        .viewportCount = 1,
        .pViewports = &viewport,
        .scissorCount = 1,
        .pScissors = &scissor
    };

    VkPipelineColorBlendStateCreateInfo color_blending = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
        .logicOpEnable = VK_FALSE,
        .logicOp = VK_LOGIC_OP_COPY,
        .attachmentCount = 1,
        .pAttachments = &color_blend_attachment,
        .blendConstants = {0.0f,0.0f,0.0f,0.0f}
    };

    VkGraphicsPipelineCreateInfo pipeline_info = {
        .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
        .stageCount = (uint32_t)shader_stages.size(),
        .pStages = shader_stages.data(),
        .pVertexInputState = &vertex_info,
        .pInputAssemblyState = &input_assembly,
        .pViewportState = &viewport_state,
        .pRasterizationState = &rasterizer,
        .pMultisampleState = &multisampling,
        .pDepthStencilState = &depth_stencil,
        .pColorBlendState = &color_blending,
        .layout = pipeline_layout,
        .renderPass = pass,
        .subpass = 0,
        .basePipelineHandle = VK_NULL_HANDLE,
        .basePipelineIndex = -1
    };

    VK_CALL(vkCreateGraphicsPipelines(
        device,
        VK_NULL_HANDLE, 1,
        &pipeline_info,
        nullptr,
        &pipeline
    ));

    return {pipeline, pipeline_layout};
}

}