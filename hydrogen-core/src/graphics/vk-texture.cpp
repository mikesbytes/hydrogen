#include "hydrogen/graphics/vk-texture.hpp"

#include <stb_image.h>

#include "hydrogen/graphics/vk-context.hpp"

namespace hgn::gfx {

Texture::Texture() :
    loaded_(false)
{}

Texture::Texture(Texture&& rhs) :
    loaded_(rhs.loaded_),
    context_(rhs.context_),
    image(rhs.image),
    image_view(rhs.image_view),
    width(rhs.width),
    height(rhs.height)
{
    rhs.loaded_ = false;
    rhs.context_ = nullptr;
    rhs.image = AllocatedImage {};
    rhs.image_view = VkImageView {};
    rhs.width = 0;
    rhs.height = 0;
}

Texture::Texture(
    vk::VkContext* context, AllocatedImage image, VkImageView image_view,
    size_t width, size_t height
) :
    context_(context),
    image(image),
    image_view(image_view),
    width(width),
    height(height)
{}

Texture::~Texture() {
    destroy_if_loaded();
}

Texture& Texture::operator=(Texture&& rhs) {
    destroy_if_loaded();

    loaded_ = rhs.loaded_;
    context_ = rhs.context_;
    image = rhs.image;
    image_view = rhs.image_view;
    width = rhs.width;
    height = rhs.height;

    rhs.loaded_ = false;
    rhs.context_ = nullptr;
    rhs.image = AllocatedImage {};
    rhs.image_view = VkImageView {};
    rhs.width = 0;
    rhs.height = 0;

    return *this;
}
bool Texture::loaded() {
    return loaded_;
}

void Texture::destroy_if_loaded() {
    if (loaded_) {
        vkDestroyImageView(*context_->get_device(), image_view, nullptr);
        vmaDestroyImage(*context_->get_allocator(), image.image, image.allocation);
    }
}

AllocatedImage create_image(vk::VkContext* ctx,
    uint32_t width, uint32_t height,
    VkFormat format, VkImageTiling tiling,
    VkImageUsageFlags usage, std::optional<VmaAllocationCreateFlagBits> alloc_flags
){
    AllocatedImage img;
    VkImageCreateInfo image_info = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        .flags = 0,
        .imageType = VK_IMAGE_TYPE_2D,
        .format = format,
        .extent = {
            .width = static_cast<uint32_t>(width),
            .height = static_cast<uint32_t>(height),
            .depth = 1
        },
        .mipLevels = 1,
        .arrayLayers = 1,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .tiling = tiling,
        .usage = usage,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
    };

    VmaAllocationCreateInfo alloc_info = {
        .usage = VMA_MEMORY_USAGE_AUTO,
    };

    if (alloc_flags.has_value()) {
        alloc_info.flags = alloc_flags.value();
    }

    VK_CALL(vmaCreateImage(
        *ctx->get_allocator(),
        &image_info,
        &alloc_info,
        &img.image,
        &img.allocation,
        nullptr
    ));

    return img;
}

Texture loadTextureFromFile(
    vk::VkContext* ctx,
    const std::string& file_name
) {
    Texture tex;
    VmaAllocator* allocator = ctx->get_allocator();
    VkFormat img_format = VK_FORMAT_R8G8B8A8_SRGB;

    int tex_w, tex_h, tex_c;
    stbi_uc* pixels = stbi_load(file_name.c_str(), &tex_w, &tex_h, &tex_c, STBI_rgb_alpha);
    VkDeviceSize image_size = tex_w * tex_h * 4;

    if (!pixels) {
        throw std::runtime_error("Failed to load texture image from file");
    }

    AllocatedBuffer staging_buffer;

    VkBufferCreateInfo buffer_info = {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = image_size,
        .usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE
    };

    VmaAllocationCreateInfo alloc_info = {
        .flags = VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT,
        .usage = VMA_MEMORY_USAGE_AUTO,
    };

    VK_CALL(vmaCreateBuffer(
        *allocator, &buffer_info, &alloc_info, &staging_buffer.buffer, &staging_buffer.allocation, nullptr
    ));

    void* data;
    vmaMapMemory(*allocator, staging_buffer.allocation, &data);
    memcpy(data, pixels, static_cast<size_t>(image_size));
    vmaUnmapMemory(*allocator, staging_buffer.allocation);

    stbi_image_free(pixels);

    AllocatedImage img = create_image( ctx,
        tex_w, tex_h,
        img_format,
        VK_IMAGE_TILING_OPTIMAL,
        VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
        std::nullopt
    );

    ctx->transition_image_layout(img.image,
    img_format, VK_IMAGE_LAYOUT_UNDEFINED,
    VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);

    ctx->copy_buffer_to_image(
        staging_buffer.buffer, img.image,
        static_cast<uint32_t>(tex_w), static_cast<uint32_t>(tex_h)
    );

    ctx->transition_image_layout(img.image,
    img_format, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
    VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    vmaDestroyBuffer(*allocator, staging_buffer.buffer, staging_buffer.allocation);

    VkImageViewCreateInfo view_info = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .image = img.image,
        .viewType = VK_IMAGE_VIEW_TYPE_2D,
        .format = img_format,
        .subresourceRange = {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1
        }
    };

    VkImageView image_view;
    VK_CALL(vkCreateImageView(*ctx->get_device(), &view_info, nullptr, &image_view));

    return Texture(ctx, img, image_view, tex_w, tex_h);
}

}