#include "hydrogen/graphics/texture-manager.hpp"

#include "hydrogen/graphics/vk-context.hpp"

namespace hgn::gfx {


void TextureManager::init(vk::VkContext* context) {
    context_ = context;
}

void TextureManager::sync_texture_sets(uint32_t frame_index) {
    auto& current_updates = texture_updates_[frame_index];
    if (current_updates.empty()) return;
    uint32_t binding_index = 0;
    uint32_t update_count = (uint32_t)current_updates.size();

    std::vector<VkWriteDescriptorSet> descriptor_writes;
    descriptor_writes.reserve(current_updates.size());

    std::vector<VkDescriptorImageInfo> image_infos;
    image_infos.reserve(current_updates.size());

    for(size_t i = 0; i < current_updates.size(); ++i) {
        auto& update = current_updates[i];

        VkDescriptorImageInfo image_info = {
            .sampler = update.sampler,
            .imageView = update.image_view,
            .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
        };
        image_infos.push_back(image_info);

        VkWriteDescriptorSet descriptor_write = {
            .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            .dstSet = update.texture_set->descriptor_sets_[frame_index],
            .dstBinding = binding_index,
            .dstArrayElement = update.texture_index,
            .descriptorCount = 1,
            .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            .pImageInfo = &image_infos.back()
        };

        descriptor_writes.push_back(descriptor_write);
    }

    vkUpdateDescriptorSets(*(context_->get_device()), update_count, descriptor_writes.data(), 0, nullptr); 
}

void TextureManager::queue_texture_update(TextureUpdate update) {
    for (auto& i : texture_updates_) {
        i.push_back(update);
    }
}

}