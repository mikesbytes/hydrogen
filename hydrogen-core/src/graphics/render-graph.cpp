#include "hydrogen/graphics/render-graph.hpp"

namespace hgn::gfx {

RenderItem* RenderGraph::add_render_object(RenderItem render_object) {
    render_items_.push_back(std::move(render_object));
    return &render_items_.back();
}

RenderList RenderGraph::get_render_list() {
    RenderList render_list;
    for (auto& obj : render_items_) {
        render_list.push_back(&obj);
    }
    return render_list;
}

};