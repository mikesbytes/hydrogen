#include "hydrogen/graphics/vk-context.hpp"

#include <fmt/format.h>
#include <spdlog/spdlog.h>
#include <set>
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <chrono>
#include <stb_image.h>
#include <imgui.h>
#include <imgui_impl_sdl2.h>
#include <imgui_impl_vulkan.h>
#include <numbers>

#include "hydrogen/utils/fileloader.hpp"
#include "hydrogen/graphics/vk-mesh.hpp"
#include "hydrogen/graphics/vk-pipeline-utils.hpp"

namespace hgn::gfx::vk {

const std::vector<const char *> VALIDATION_LAYERS = {
    "VK_LAYER_KHRONOS_validation"
};

const std::vector<const char*> DEVICE_EXTENSIONS = {
    VK_KHR_SWAPCHAIN_EXTENSION_NAME,
    VK_KHR_SHADER_DRAW_PARAMETERS_EXTENSION_NAME
};

#ifndef NDEBUG
    const bool ENABLE_VALIDATION_LAYERS = true;
#else
    const bool ENABLE_VALIDATION_LAYERS = false;
#endif

const int MAX_FRAMES_IN_FLIGHT = 2;
const int MAX_SCENE_OBJECTS = 100;
const int MAX_SCENE_LIGHTS = 100;


// holds global data that changes scene to scene

struct FrameUBO {
    glm::mat4 model;
    glm::mat4 view;
    glm::mat4 proj;
    glm::vec3 light_pos;
    glm::vec3 camera_pos;
    uint32_t light_count;
};

struct SceneObjectSBO {
    glm::mat4 model_mat;
};

struct SceneLightSBO {
    glm::vec3 light_pos;
    glm::vec3 light_color;
    float light_value;
};

VkResult createDebugUtilsMessengerEXT(
    VkInstance instance,
    const VkDebugUtilsMessengerCreateInfoEXT* p_create_info,
    const VkAllocationCallbacks* p_allocator,
    VkDebugUtilsMessengerEXT* p_debug_messenger
){
    auto func = (PFN_vkCreateDebugUtilsMessengerEXT) vkGetInstanceProcAddr(
        instance, "vkCreateDebugUtilsMessengerEXT"
    );
    if (func != nullptr) {
        return func(instance, p_create_info, p_allocator, p_debug_messenger);
    } else {
        spdlog::error("Debugging Extension Not Present");
        return VK_ERROR_EXTENSION_NOT_PRESENT;
    }
}

void destroyDebugUtilsMessengerEXT(
    VkInstance instance,
    VkDebugUtilsMessengerEXT debug_messenger,
    const VkAllocationCallbacks* p_allocator
){
    auto func = (PFN_vkDestroyDebugUtilsMessengerEXT) vkGetInstanceProcAddr(
        instance,
        "vkDestroyDebugUtilsMessengerEXT"
    );
    if (func != nullptr) {
        func(instance, debug_messenger, p_allocator);
    }
}

void VkContext::init_vulkan() {
    if (ENABLE_VALIDATION_LAYERS) {
        spdlog::info("VALIDATION LAYERS ENABLED");
    }
    init_window();
    create_instance();
    setup_debug_messenger();
    create_surface();
    pick_physical_device();
    create_logical_device();
    create_allocator();
    create_swap_chain();
    create_image_views();
    create_render_pass();
    create_depth_resources();
    create_frame_buffers();
    create_command_pool();
    create_command_buffers();
    create_sync_objects();

    // Done with initial setup here

    init_imgui();
    create_texture_image();
    create_texture_sampler();
    create_vertex_buffer();
    create_uniform_buffers();
    create_descriptor_set_layout();
    create_graphics_pipeline();
    current_frame_ = 0;
}

void VkContext::init_window() {
    SDL_Init(SDL_INIT_VIDEO);

    window_ = SDL_CreateWindow("HYDROGEN", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1920, 1080, SDL_WINDOW_SHOWN | SDL_WINDOW_VULKAN);
}

void VkContext::create_instance() {
    auto extensions = get_required_extensions();

    VkApplicationInfo app_info = {
        .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
        .pApplicationName = "Hydrogen Demo",
        .applicationVersion = VK_MAKE_VERSION(0, 0, 1),
        .pEngineName = "Hydrogen",
        .engineVersion = VK_MAKE_VERSION(0, 0, 1),
        .apiVersion = VK_API_VERSION_1_3
    };

    VkInstanceCreateInfo inst_info = {
        VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,     // sType
        nullptr,                                    // pNext
        0,                                          // flags
        &app_info,                                    // pApplicationInfo
        0,                                          // enabledLayerCount
        nullptr,                                    // ppEnabledLayerNames
        static_cast<uint32_t>(extensions.size()),   // enabledExtensionCount
        extensions.data()                      // ppEnabledExtensionNames
    };

    if (ENABLE_VALIDATION_LAYERS && !check_validation_layer_support()) {
        throw std::runtime_error("Vulkan validation layers enabled but not available");
    }

    if (ENABLE_VALIDATION_LAYERS) {
        inst_info.enabledLayerCount = static_cast<uint32_t>(VALIDATION_LAYERS.size());
        inst_info.ppEnabledLayerNames = VALIDATION_LAYERS.data();
    }

    VkResult res;
    res = vkCreateInstance(&inst_info, nullptr, &instance_);
    if (res != VK_SUCCESS) {
        throw std::runtime_error(fmt::format("vkCreateInstance error: {}", res));
    }

}

void VkContext::loop() {
    bool running = true;
    while (running) {
        // process events
        // GUI STUFF

        // start gui frame
        ImGui::ShowDemoWindow();

        // end gui frame
        // draw frame
        draw_frame();
    }
}

void VkContext::process_events(std::function<void(SDL_Event)> event_callback) {
    SDL_Event evt;
    while (SDL_PollEvent(&evt)) {
        ImGui_ImplSDL2_ProcessEvent(&evt);
        if (evt.type == SDL_WINDOWEVENT_RESIZED) {
            frame_buffer_resized_ = true;
        }
        event_callback(evt);
    }
}

void VkContext::start_gui_frame() {
    ImGui_ImplVulkan_NewFrame();
    ImGui_ImplSDL2_NewFrame(window_);

    ImGui::NewFrame();
}

void VkContext::end_gui_frame() {
    ImGui::Render();

}

void VkContext::cleanup(){
    vkDeviceWaitIdle(device_);
    cleanup_swap_chain();
    vkDestroySampler(device_, texture_sampler_, nullptr);
    //vkDestroyImageView(device_, texture_image_view_, nullptr);
    //vkDestroyImage(device_, texture_image_, nullptr);
    //vkFreeMemory(device_, texture_image_memory_, nullptr);
    for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i) {
        vmaDestroyBuffer(allocator_, uniform_buffers_[i], uniform_buffer_memories_[i]);
        vmaDestroyBuffer(allocator_, scene_obj_buffers_[i].buffer, scene_obj_buffers_[i].allocation);
        vmaDestroyBuffer(allocator_, scene_light_buffers_[i].buffer, scene_light_buffers_[i].allocation);
    }
    //vkDestroyDescriptorPool(device_, descriptor_pool_, nullptr);
    //vkDestroyDescriptorSetLayout(device_, descriptor_set_layout_, nullptr);
    //vmaDestroyBuffer(allocator_, vertex_buffer_, nullptr);
    vmaDestroyBuffer(allocator_, index_buffer_, index_buffer_memory_);
    for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i) {
        vkDestroySemaphore(device_, image_available_sems_[i], nullptr);
        vkDestroySemaphore(device_, render_finished_sems_[i], nullptr);
        vkDestroyFence(device_, in_flight_fences_[i], nullptr);
    }
    vkDestroyCommandPool(device_, command_pool_, nullptr);
    vkDestroyPipeline(device_, pipeline_, nullptr);
    vkDestroyPipelineLayout(device_, pipeline_layout_, nullptr);
    vkDestroyRenderPass(device_, render_pass_, nullptr);
    vmaDestroyAllocator(allocator_);
    vkDestroyDevice(device_, nullptr);
    if (ENABLE_VALIDATION_LAYERS) {
        destroyDebugUtilsMessengerEXT(instance_, debug_messenger_, nullptr);
    }
    vkDestroySurfaceKHR(instance_, surface_, nullptr);
    vkDestroyInstance(instance_, nullptr);
    SDL_DestroyWindow(window_);
    SDL_Quit();
}

VkDevice* VkContext::get_device() {
    return &device_;
}

VmaAllocator* VkContext::get_allocator() {
    return &allocator_;
}

bool VkContext::QueueFamilyIndices_::is_complete() {
    return graphics_family.has_value() && present_family.has_value();
}

bool VkContext::check_validation_layer_support() {
    uint32_t layer_count;
    vkEnumerateInstanceLayerProperties(&layer_count, nullptr);

    std::vector<VkLayerProperties> available_layers(layer_count);
    vkEnumerateInstanceLayerProperties(&layer_count, available_layers.data());

    for (const char* layer_name : VALIDATION_LAYERS) {
        bool layer_found = false;

        for (const auto& layer_props : available_layers) {
            if (strcmp(layer_name, layer_props.layerName) == 0) {
                layer_found = true;
                break;
            }
        }

        if (!layer_found) return false;
    }

    return true;
}

std::vector<const char *> VkContext::get_required_extensions()
{
    uint32_t extension_count;
    if (SDL_Vulkan_GetInstanceExtensions(window_, &extension_count, nullptr) == SDL_FALSE) {
        throw std::runtime_error(fmt::format("SDL_Vulkan_GetInstanceExtensions error: {}", SDL_GetError()));
    }
    
    std::vector<const char*> extension_names(extension_count);
    if (SDL_Vulkan_GetInstanceExtensions(window_, &extension_count, extension_names.data()) == SDL_FALSE) {
        throw std::runtime_error(fmt::format("SDL_Vulkan_GetInstanceExtensions error: {}", SDL_GetError()));
    }

    if (ENABLE_VALIDATION_LAYERS)
        extension_names.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);

    return extension_names;
}

VKAPI_ATTR VkBool32 VKAPI_CALL VkContext::debug_callback(VkDebugUtilsMessageSeverityFlagBitsEXT message_severity, VkDebugUtilsMessageTypeFlagsEXT message_type, const VkDebugUtilsMessengerCallbackDataEXT *p_callback_data, void *p_user_data)
{
    spdlog::error("Validation Layer: {}\n", p_callback_data->pMessage);
    return VK_FALSE;
}

void VkContext::setup_debug_messenger() {
    if (!ENABLE_VALIDATION_LAYERS) return;

    VkDebugUtilsMessengerCreateInfoEXT create_info = {
        VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,    // sType
        nullptr,                                                    // pNext
        0,                                                          // flags
        VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |           // messageSeverity
            VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
            VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,
        VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |               // messageType
            VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
            VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT,
        VkContext::debug_callback,                                     // pfnUserCallback
        nullptr                                                     // pUserData
    };

    if (createDebugUtilsMessengerEXT(instance_, &create_info, nullptr, &debug_messenger_) != VK_SUCCESS) {
        throw std::runtime_error("Failed to set up VK debug messenger");
    }
}

void VkContext::pick_physical_device() {
    physical_device_ = VK_NULL_HANDLE;

    uint32_t physical_device__count;
    vkEnumeratePhysicalDevices(instance_, &physical_device__count, nullptr);
    std::vector<VkPhysicalDevice> physical_device_s(physical_device__count);
    vkEnumeratePhysicalDevices(instance_, &physical_device__count, physical_device_s.data());
    for (const auto& device : physical_device_s) {
        if (is_device_suitable(device)) {
            physical_device_ = device;
            break;
        }
    }

    if (physical_device_ == VK_NULL_HANDLE) {
        throw std::runtime_error("Failed to find a suitable GPU");
    }
}

bool VkContext::is_device_suitable(VkPhysicalDevice device) {
    VkPhysicalDeviceProperties device_props;
    VkPhysicalDeviceFeatures device_features;
    vkGetPhysicalDeviceProperties(device, &device_props);
    vkGetPhysicalDeviceFeatures(device, &device_features);

    return true;
}

VkContext::QueueFamilyIndices_ VkContext::find_queue_families(VkPhysicalDevice device) {
    QueueFamilyIndices_ indices;
    
    uint32_t queue_family_count = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queue_family_count, nullptr);

    std::vector<VkQueueFamilyProperties> queue_families(queue_family_count);
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queue_family_count, queue_families.data());

    int i = 0;
    for (const auto& queue_family : queue_families) {
        if (queue_family.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
            indices.graphics_family = i;
        }

        VkBool32 present_support = false;
        vkGetPhysicalDeviceSurfaceSupportKHR(physical_device_, i, surface_, &present_support);
        if (present_support) {
            indices.present_family = i;
        }

        ++i;
    }

    return indices;
}

void VkContext::create_logical_device() {
    QueueFamilyIndices_ indices = find_queue_families(physical_device_);

    float queue_priority = 1.0f;
    std::vector<VkDeviceQueueCreateInfo> queue_create_infos;
    std::set<uint32_t> unique_queue_families = {
        indices.graphics_family.value(),
        indices.present_family.value()
    };

    for (uint32_t queue_family : unique_queue_families) {
        VkDeviceQueueCreateInfo queue_info = {
            VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,     // sType
            nullptr,                                        // pNext
            0,                                              // ??
            queue_family,                                   // queueFamilyIndex
            1,                                              // queueCount
            &queue_priority                                 // queuePriority
        };
        queue_create_infos.push_back(queue_info);
    }


    VkPhysicalDeviceFeatures device_features = {
        .samplerAnisotropy = VK_TRUE,
    };

    VkPhysicalDeviceDescriptorIndexingFeatures indexing_features = {
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_FEATURES,
        .shaderSampledImageArrayNonUniformIndexing = VK_TRUE,
        .shaderStorageBufferArrayNonUniformIndexing = VK_TRUE,
        .shaderStorageImageArrayNonUniformIndexing = VK_TRUE,
        .descriptorBindingSampledImageUpdateAfterBind = VK_TRUE,
        .descriptorBindingStorageImageUpdateAfterBind = VK_TRUE,
        .descriptorBindingStorageBufferUpdateAfterBind = VK_TRUE,
        .descriptorBindingPartiallyBound = VK_TRUE,
        .descriptorBindingVariableDescriptorCount = VK_TRUE,
        .runtimeDescriptorArray = VK_TRUE,

    };

    VkPhysicalDeviceShaderDrawParameterFeatures draw_param_features = {
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_DRAW_PARAMETER_FEATURES,
        .pNext = &indexing_features,
        .shaderDrawParameters = VK_TRUE
    };

    VkDeviceCreateInfo create_info = {
        VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,               // sType
        &draw_param_features,                                            // pNext
        0,                                                  // flags
        static_cast<uint32_t>(queue_create_infos.size()),   // queueCreateInfoCount
        queue_create_infos.data(),                          // pQueueCreateInfos
        0,                                                  // enabledLayerCount
        nullptr,                                            // ppEnabledLayerNames
        static_cast<uint32_t>(DEVICE_EXTENSIONS.size()),    // enabledExtensionCount
        DEVICE_EXTENSIONS.data(),                           // ppEnabledExtensionNames
        &device_features                                    // pEnabledFeatures
    };

    if (vkCreateDevice(physical_device_, &create_info, nullptr, &device_) != VK_SUCCESS) {
        throw std::runtime_error("Failed to create VK logical device");
    }

    vkGetDeviceQueue(device_, indices.graphics_family.value(), 0, &graphics_queue_);
    vkGetDeviceQueue(device_, indices.present_family.value(), 0, &present_queue_);
}

void VkContext::create_surface() {
    if (SDL_Vulkan_CreateSurface(window_, instance_, &surface_) != SDL_TRUE) {
        throw std::runtime_error("SDL2 failed to create VK surface");
    }
}

bool VkContext::check_device_extension_support(VkPhysicalDevice device) {
    uint32_t extension_count;
    vkEnumerateDeviceExtensionProperties(device, nullptr, &extension_count, nullptr);

    std::vector<VkExtensionProperties> available_extensions(extension_count);
    vkEnumerateDeviceExtensionProperties(device, nullptr, &extension_count, available_extensions.data());

    std::set<std::string> required_extensions(DEVICE_EXTENSIONS.begin(), DEVICE_EXTENSIONS.end());
    for (const auto& extension : available_extensions) {
        required_extensions.erase(extension.extensionName);
    }

    return required_extensions.empty();
}

VkContext::SwapChainSupportDetails VkContext::query_swap_chain_support(VkPhysicalDevice device) {
    SwapChainSupportDetails details;
    
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface_, &details.capabilities);

    uint32_t format_count;
    vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface_, &format_count, nullptr);

    if (format_count != 0) {
        details.formats.resize(format_count);
        vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface_, &format_count, details.formats.data());
    }

    uint32_t present_mode_count;
    vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface_, &present_mode_count, nullptr);

    if (present_mode_count != 0) {
        details.present_modes.resize(present_mode_count);
        vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface_, &present_mode_count, details.present_modes.data());
    }

    return details;
}

VkSurfaceFormatKHR VkContext::choose_swap_surface_format(const std::vector<VkSurfaceFormatKHR> &available_formats) {
    for (const auto& available_format : available_formats) {
        if (
            available_format.format == VK_FORMAT_B8G8R8A8_SRGB &&
            available_format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR
        ) {
            return available_format;
        }
    }
    return available_formats[0];
}

VkPresentModeKHR VkContext::choose_swap_present_mode(const std::vector<VkPresentModeKHR> &available_modes)
{
    for (const auto& mode : available_modes) {
        if (mode == VK_PRESENT_MODE_MAILBOX_KHR) {
            return mode;
        }
    }
    return VK_PRESENT_MODE_FIFO_KHR;
}

VkExtent2D VkContext::choose_swap_extent(const VkSurfaceCapabilitiesKHR& capabilities) {
    if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) {
        return capabilities.currentExtent;
    } else {
        int w, h;
        SDL_Vulkan_GetDrawableSize(window_, &w, &h);

        VkExtent2D extent = {
            static_cast<uint32_t>(w),
            static_cast<uint32_t>(h)
        };

        extent.width = std::clamp(
            extent.width,
            capabilities.minImageExtent.width,
            capabilities.maxImageExtent.width);
        extent.height = std::clamp(
            extent.height,
            capabilities.minImageExtent.height,
            capabilities.maxImageExtent.height);
        return extent;
    }
}

void VkContext::create_swap_chain() {
    SwapChainSupportDetails swap_chain_support = query_swap_chain_support(physical_device_);

    VkSurfaceFormatKHR surface_format = choose_swap_surface_format(swap_chain_support.formats);
    VkPresentModeKHR presentMode = choose_swap_present_mode(swap_chain_support.present_modes);
    VkExtent2D extent = choose_swap_extent(swap_chain_support.capabilities);

    uint32_t image_count = swap_chain_support.capabilities.minImageCount + 1;
    if (
        swap_chain_support.capabilities.maxImageCount > 0 &&
        image_count > swap_chain_support.capabilities.maxImageCount
    ) {
        image_count = swap_chain_support.capabilities.maxImageCount;
    }

    QueueFamilyIndices_ indices = find_queue_families(physical_device_);
    uint32_t qf_indices[] = {indices.graphics_family.value(), indices.present_family.value()};

    VkSwapchainCreateInfoKHR create_info = {
        VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,    // sType
        nullptr,                                        // pNext
        0,                                              // flags
        surface_,                                       // surface
        image_count,                                    // minImageCount
        surface_format.format,                          // imageFormat
        surface_format.colorSpace,                      // imageColorSpace
        extent,                                         // imageExtent
        1,                                              // imageArrayLayers
        VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,            // imageUsage
        VK_SHARING_MODE_CONCURRENT,                     // imageSharingMode
        2,                                              // queueFamilyIndexcount
        qf_indices,                                     // pQueueFamilyIndices
        swap_chain_support.capabilities.currentTransform, // preTransform
        VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,              // compositeAlpha
        presentMode,                                    // presentMode
        VK_TRUE,                                        // clipped
        VK_NULL_HANDLE                                  // oldSwapchain
    };

    // check that we can share stuff
    if (indices.graphics_family == indices.present_family) {
        create_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        create_info.queueFamilyIndexCount = 0;
        create_info.pQueueFamilyIndices = nullptr;
    }

    if (vkCreateSwapchainKHR(
        device_,
        &create_info,
        nullptr,
        &swap_chain_
    ) != VK_SUCCESS) {
        throw std::runtime_error("Failed to create VK swap chain");
    }

    vkGetSwapchainImagesKHR(device_, swap_chain_, &image_count, nullptr);
    swap_chain_images_.resize(image_count);
    vkGetSwapchainImagesKHR(device_, swap_chain_, &image_count, swap_chain_images_.data());

    swap_chain_image_format_ = surface_format.format;
    extent_ = extent;
}

void VkContext::create_image_views() {
    swap_chain_image_views_.resize(swap_chain_images_.size());
    for (size_t i = 0; i < swap_chain_images_.size(); ++i) {
        VkImageViewCreateInfo create_info = {
            VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,   // sType
            nullptr,                                    // pNext
            0,                                          // flags
            swap_chain_images_[i],                      // image
            VK_IMAGE_VIEW_TYPE_2D,                      // viewType
            swap_chain_image_format_,                   // format
            VkComponentMapping{                         // components
                VK_COMPONENT_SWIZZLE_IDENTITY,
                VK_COMPONENT_SWIZZLE_IDENTITY,
                VK_COMPONENT_SWIZZLE_IDENTITY,
                VK_COMPONENT_SWIZZLE_IDENTITY
            },
            VkImageSubresourceRange{
                VK_IMAGE_ASPECT_COLOR_BIT,
                0,
                1,
                0,
                1
            }
        };
        if (vkCreateImageView(device_, &create_info, nullptr, &swap_chain_image_views_[i]) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create image views");
        }
    }
}

void VkContext::create_graphics_pipeline() {
    auto vert_code = hgn::utils::read_binary_file("shaders/vert.spv");
    auto frag_code = hgn::utils::read_binary_file("shaders/frag.spv");

    VkShaderModule vert_module = create_shader_module(vert_code);
    VkShaderModule frag_module = create_shader_module(frag_code);

    auto descriptions = Vertex::get_vertex_description();
    std::vector<VkDescriptorSetLayout> descriptor_layouts = { descriptor_set_layout_, scene_data_descriptor_set_layout_ };

    auto [ pipeline, layout ] = gfx::PipelineBuilder()
        .add_shader_stage(VK_SHADER_STAGE_VERTEX_BIT, vert_module)
        .add_shader_stage(VK_SHADER_STAGE_FRAGMENT_BIT, frag_module)
        .create_vertex_info(descriptions.bindings, descriptions.attributes)
        .create_input_assembly_info(VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST)
        .create_rasterizer(VK_POLYGON_MODE_FILL)
        .create_viewport(
            0.0f, 0.0f,
            (float)extent_.width, (float)extent_.height,
            0.0f, 1.0f
        )
        .create_scissor(0,0,extent_.width, extent_.height)
        .create_multisampling()
        .create_color_blend_attachment()
        .create_depth_stencil()
        .create_layout(descriptor_layouts)
        .build_pipeline(device_, render_pass_);
    
    pipeline_ = pipeline;
    pipeline_layout_ = layout;

    // don't put anything else after this
    vkDestroyShaderModule(device_, vert_module, nullptr);
    vkDestroyShaderModule(device_, frag_module, nullptr);
}

VkShaderModule VkContext::create_shader_module(const std::vector<char> &data) {
    VkShaderModuleCreateInfo create_info = {
        VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,    // sType
        nullptr,                                        // pNext
        0,                                              // flags
        data.size(),                                    // codeSize
        reinterpret_cast<const uint32_t*>(data.data())  // pCode
    };

    VkShaderModule module;
    if (vkCreateShaderModule(device_, &create_info, nullptr, &module) != VK_SUCCESS) {
        throw std::runtime_error("Failed to create shader module");
    }

    return module;
}

void VkContext::create_render_pass() {
    VkAttachmentDescription color_attachment = {
        .format = swap_chain_image_format_,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
    };

    VkAttachmentReference color_attachment_ref = {
        .attachment = 0,
        .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
    };

    VkAttachmentDescription depth_attachment = {
        .format = find_depth_format(),
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
    };

    VkAttachmentReference depth_attachment_ref = {
        .attachment = 1,
        .layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
    };

    VkSubpassDescription subpass = {
        .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
        .colorAttachmentCount = 1,
        .pColorAttachments = &color_attachment_ref,
        .pDepthStencilAttachment = &depth_attachment_ref,
    };

    VkSubpassDependency dependency = {
        .srcSubpass = VK_SUBPASS_EXTERNAL,
        .dstSubpass = 0,
        .srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT,
        .dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT,
        .srcAccessMask = 0,
        .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
    };

    std::array<VkAttachmentDescription, 2> attachments = {
        color_attachment,
        depth_attachment
    };

    VkRenderPassCreateInfo render_pass_info = {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
        .attachmentCount = static_cast<uint32_t>(attachments.size()),
        .pAttachments = attachments.data(),
        .subpassCount = 1,
        .pSubpasses = &subpass,
        .dependencyCount = 1,
        .pDependencies = &dependency
    };

    if (vkCreateRenderPass(device_, &render_pass_info, nullptr, &render_pass_) != VK_SUCCESS) {
        throw std::runtime_error("Failed to create VK render pass");
    }

}

void VkContext::create_frame_buffers() {
    swap_chain_framebuffers_.resize(swap_chain_image_views_.size());
    for (size_t i = 0; i < swap_chain_image_views_.size(); ++i) {
        std::array<VkImageView, 2> attachments = {
            swap_chain_image_views_[i],
            depth_image_view_
        };

        VkFramebufferCreateInfo framebuffer_info = {
            .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
            .renderPass = render_pass_,
            .attachmentCount = static_cast<uint32_t>(attachments.size()),
            .pAttachments = attachments.data(),
            .width = extent_.width,
            .height = extent_.height,
            .layers = 1
        };

        if (vkCreateFramebuffer(
            device_, &framebuffer_info, nullptr, &swap_chain_framebuffers_[i]
        ) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create VK framebuffer");
        }
    }
}

void VkContext::create_command_pool() {
    QueueFamilyIndices_ qf_indices = find_queue_families(physical_device_);

    VkCommandPoolCreateInfo pool_info = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
        .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
        .queueFamilyIndex = qf_indices.graphics_family.value()
    };

    if (vkCreateCommandPool(device_, &pool_info, nullptr, &command_pool_) != VK_SUCCESS) {
        throw std::runtime_error("Failed to create VK command pool");
    }
}

void VkContext::create_command_buffers() {
    command_buffers_.resize(MAX_FRAMES_IN_FLIGHT);

    VkCommandBufferAllocateInfo alloc_info = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .commandPool = command_pool_,
        .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandBufferCount = (uint32_t) command_buffers_.size()
    };

    if (vkAllocateCommandBuffers(device_, &alloc_info, command_buffers_.data()) != VK_SUCCESS) {
        throw std::runtime_error("Failed to allocate command buffer");
    }
}

void VkContext::record_command_buffer(VkCommandBuffer buffer, uint32_t image_index) {
    VkCommandBufferBeginInfo begin_info = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .flags = 0,
        .pInheritanceInfo = nullptr
    };

    if (vkBeginCommandBuffer(buffer, &begin_info) != VK_SUCCESS) {
        throw std::runtime_error("Failed to begin recording VK command buffer");
    }

    std::array<VkClearValue, 2> clear_values = {
        VkClearValue{.color = {0.0f, 0.0f, 0.0f, 1.0f}},
        VkClearValue{.depthStencil = {1.0f, 0}},
    };
    VkRenderPassBeginInfo render_pass_info = {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
        .renderPass = render_pass_,
        .framebuffer = swap_chain_framebuffers_[image_index],
        .renderArea = {
            .offset = {0, 0},
            .extent = extent_
        },
        .clearValueCount = static_cast<uint32_t>(clear_values.size()),
        .pClearValues = clear_values.data()
    };
    vkCmdBeginRenderPass(buffer, &render_pass_info, VK_SUBPASS_CONTENTS_INLINE);
    vkCmdBindPipeline(buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline_);
    VkViewport viewport = {
        .x = 0.0f,
        .y = 0.0f,
        .width = static_cast<float>(extent_.width),
        .height = static_cast<float>(extent_.height),
        .minDepth = 0.0f,
        .maxDepth = 1.0f
    };
    //vkCmdSetViewport(buffer, 0, 1, &viewport);

    /*
    VkRect2D scissor = {
        .offset = {0, 0},
        .extent = extent_
    };
    vkCmdSetScissor(buffer, 0, 1, &scissor);
    */
    
    vkCmdBindDescriptorSets(buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline_layout_,
        0, 1, &descriptor_sets_[current_frame_], 0, nullptr);
    vkCmdBindDescriptorSets(buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline_layout_,
        1, 1, &scene_data_descriptor_sets_[current_frame_], 0, nullptr);

    // draw objects
    auto render_list = render_graph_.get_render_list();

    for (size_t i = 0; i < render_list.size(); ++i) {
        VkBuffer vertex_buffers[] = {*(render_list[i]->mesh.vertex_buffer.get_buffer())};
        VkDeviceSize offsets[] = {0};
        vkCmdBindVertexBuffers(buffer, 0, 1, vertex_buffers, offsets);
        vkCmdBindIndexBuffer(buffer, *(render_list[i]->mesh.index_buffer.get_buffer()), 0, VK_INDEX_TYPE_UINT32);

        vkCmdDrawIndexed(buffer, static_cast<uint32_t>(render_list[i]->mesh.index_count()), 1, 0, 0, i);
    }

    ImGui_ImplVulkan_RenderDrawData(ImGui::GetDrawData(), buffer);

    vkCmdEndRenderPass(buffer);

    if (vkEndCommandBuffer(buffer) != VK_SUCCESS) {
        throw std::runtime_error("Failed to record VK command buffer");
    }
}

void VkContext::create_sync_objects() {
    image_available_sems_.resize(MAX_FRAMES_IN_FLIGHT);
    render_finished_sems_.resize(MAX_FRAMES_IN_FLIGHT);
    in_flight_fences_.resize(MAX_FRAMES_IN_FLIGHT);

    VkSemaphoreCreateInfo sem_info = {
        .sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO
    };

    VkFenceCreateInfo fence_info = {
        .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
        .flags = VK_FENCE_CREATE_SIGNALED_BIT
    };

    for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i){
        if (
            vkCreateSemaphore(device_, &sem_info, nullptr, &image_available_sems_[i]) != VK_SUCCESS ||
            vkCreateSemaphore(device_, &sem_info, nullptr, &render_finished_sems_[i]) != VK_SUCCESS ||
            vkCreateFence(device_, &fence_info, nullptr, &in_flight_fences_[i]) != VK_SUCCESS
        ){
            throw std::runtime_error("Failed to create VK semaphores");
        }
    }
}

void VkContext::draw_frame() {
    vkWaitForFences(device_, 1, &in_flight_fences_[current_frame_], VK_TRUE, UINT64_MAX);
    VkResult result = vkAcquireNextImageKHR(
        device_, swap_chain_, UINT64_MAX, image_available_sems_[current_frame_], VK_NULL_HANDLE, &swap_image_index_
    );

    if (result == VK_ERROR_OUT_OF_DATE_KHR) {
        recreate_swap_chain();
        return;
    } else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
        throw std::runtime_error("Failed to acquire VK swap chain image");
    }

    vkResetFences(device_, 1, &in_flight_fences_[current_frame_]);


    vkResetCommandBuffer(command_buffers_[current_frame_], 0);
    record_command_buffer(command_buffers_[current_frame_], swap_image_index_);

    //update_uniform_buffer(current_frame_);

}

void VkContext::sync_frame_data() {
    update_uniform_buffer(current_frame_);
}

void VkContext::present_frame() {
    VkSemaphore wait_sems[] = {image_available_sems_[current_frame_]};
    VkSemaphore sig_sems[] = {render_finished_sems_[current_frame_]};
    VkPipelineStageFlags wait_stages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};

    VkSubmitInfo submit_info = {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .waitSemaphoreCount = 1,
        .pWaitSemaphores = wait_sems,
        .pWaitDstStageMask = wait_stages,
        .commandBufferCount = 1,
        .pCommandBuffers = &command_buffers_[current_frame_],
        .signalSemaphoreCount = 1,
        .pSignalSemaphores = sig_sems
    };
    if (vkQueueSubmit(graphics_queue_, 1, &submit_info, in_flight_fences_[current_frame_]) != VK_SUCCESS) {
        throw std::runtime_error("Failed to submit draw command buffer");
    }

    VkSwapchainKHR swap_chains[] = {swap_chain_};
    VkPresentInfoKHR present_info = {
        .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
        .waitSemaphoreCount = 1,
        .pWaitSemaphores = sig_sems,
        .swapchainCount = 1,
        .pSwapchains = swap_chains,
        .pImageIndices = &swap_image_index_,
        .pResults = nullptr
    };

    VkResult result = vkQueuePresentKHR(present_queue_, &present_info); 
    if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || frame_buffer_resized_) {
        recreate_swap_chain();
        frame_buffer_resized_ = false;
    } else if (result != VK_SUCCESS) {
        throw std::runtime_error("Failed to present VK swap chain image");
    }
    current_frame_ = (current_frame_ + 1) % MAX_FRAMES_IN_FLIGHT;
}

void VkContext::cleanup_swap_chain() {
    vkDestroyImageView(device_, depth_image_view_, nullptr);
    vkDestroyImage(device_, depth_image_, nullptr);
    vkFreeMemory(device_, depth_image_memory_, nullptr);

    for (auto framebuffer : swap_chain_framebuffers_) {
        vkDestroyFramebuffer(device_, framebuffer, nullptr);
    }
    for (auto image_view : swap_chain_image_views_) {
        vkDestroyImageView(device_, image_view, nullptr);
    }
    vkDestroySwapchainKHR(device_, swap_chain_, nullptr);
}

void VkContext::immediate_submit(std::function<void(VkCommandBuffer)>&& fn) {


    VkCommandBufferAllocateInfo alloc_info = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .commandPool = command_pool_,
        .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandBufferCount = 1
    };

    VkCommandBuffer buffer;

    VK_CALL(vkAllocateCommandBuffers(device_, &alloc_info, &buffer));

    VkCommandBufferBeginInfo begin_info = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .pNext = nullptr,
        .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
        .pInheritanceInfo = nullptr,
    };

    VK_CALL(vkBeginCommandBuffer(buffer, &begin_info));

    fn(buffer);

    VK_CALL(vkEndCommandBuffer(buffer));

    VkFenceCreateInfo fence_info = {
        .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO
    };
    VkFence fence;
    VK_CALL(vkCreateFence(device_, &fence_info, nullptr, &fence));

    VkSubmitInfo submit_info = {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .commandBufferCount = 1,
        .pCommandBuffers = &buffer
    };

    VK_CALL(vkQueueSubmit(graphics_queue_, 1, &submit_info, fence));
    VK_CALL(vkWaitForFences(device_, 1, &fence, true, 999999999999));

    vkDestroyFence(device_, fence, nullptr);

}

void VkContext::frame_submit(std::function<void(VkCommandBuffer)>&& fn) {
    fn(command_buffers_[current_frame_]);
}


uint32_t VkContext::get_current_frame() {
    return current_frame_;
}


void VkContext::recreate_swap_chain() {
    /*
    int width = 0, height = 0;
    while (width == 0 || height == 0) {
        
    }
    */
    spdlog::debug("Recreating VK swap chain");
    vkDeviceWaitIdle(device_);
    cleanup_swap_chain();
    create_swap_chain();
    create_image_views();
    create_depth_resources();
    create_frame_buffers();
}

void VkContext::create_vertex_buffer() {
    /*
    auto mesh = gfx::loadMeshFromOBJ(this, "assets/suzanne.obj");
    render_graph_.add_render_object(
        gfx::RenderItem {
            .mesh = gfx::loadMeshFromOBJ(this, "assets/groundplane.obj"),
            .model_mat = glm::mat4(1.0f)
        }
    );

    RenderItem* item = render_graph_.add_render_object(
        gfx::RenderItem {
            .mesh = gfx::loadMeshFromOBJ(this, "assets/suzanne.obj"),
            .model_mat = glm::mat4(1.0f)
        }
    );
    render_obj_.set_render_item(item);
    render_obj_.set_rotation({
        0.5f * std::numbers::pi,
        0.0f,
        0.5f * std::numbers::pi});
    */
}

uint32_t VkContext::find_memory_type(uint32_t type_filter, VkMemoryPropertyFlags props) {
    VkPhysicalDeviceMemoryProperties mem_props;
    vkGetPhysicalDeviceMemoryProperties(physical_device_, &mem_props);

    for (uint32_t i = 0; i < mem_props.memoryTypeCount; ++i) {
        if (
            (type_filter & (1 << i)) &&
            (mem_props.memoryTypes[i].propertyFlags & props) == props
        ) {
            return i;
        }
    }

    throw std::runtime_error("Failed to find suitable VK memory type");
}

void VkContext::create_buffer(
    VkDeviceSize size, VkBufferUsageFlags usage, std::optional<VmaAllocationCreateFlagBits> properties,
    VkBuffer* buffer, VmaAllocation* buffer_memory
){
    VkBufferCreateInfo buffer_info = {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = size,
        .usage = usage,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE
    };

    if (vkCreateBuffer(device_, &buffer_info, nullptr, buffer) != VK_SUCCESS) {
        throw std::runtime_error("Failed to create VK vertex buffer");
    }

    VmaAllocationCreateInfo alloc_info = {
        .usage = VMA_MEMORY_USAGE_AUTO,
    };

    if (properties.has_value()) {
        alloc_info.flags = properties.value();
    }

    if (vmaCreateBuffer(allocator_, &buffer_info, &alloc_info, buffer, buffer_memory, nullptr) != VK_SUCCESS) {
        throw std::runtime_error("Failed to allocate VK vertex buffer memory");
    }
}

void VkContext::copy_buffer(VkBuffer src_buffer, VkBuffer dst_buffer, VkDeviceSize size) {
    immediate_submit([&](VkCommandBuffer command_buffer){
        VkBufferCopy copy_region = {
            .srcOffset = 0,
            .dstOffset = 0,
            .size = size
        };
        vkCmdCopyBuffer(command_buffer, src_buffer, dst_buffer, 1, &copy_region);
    });

    /*
    VkCommandBuffer command_buffer = begin_single_time_commands();

    VkBufferCopy copy_region = {
        .srcOffset = 0,
        .dstOffset = 0,
        .size = size
    };
    vkCmdCopyBuffer(command_buffer, src_buffer, dst_buffer, 1, &copy_region);

    end_single_time_commands(command_buffer);
    */
}

void VkContext::create_descriptor_set_layout() {
    descriptor_allocator_.init(device_);
    descriptor_layout_cache_.init(device_);

    descriptor_sets_.reserve(MAX_FRAMES_IN_FLIGHT);
    scene_data_descriptor_sets_.reserve(MAX_FRAMES_IN_FLIGHT);

    for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i) {
        VkDescriptorBufferInfo buffer_info = {
            .buffer = uniform_buffers_[i],
            .offset = 0,
            .range = sizeof(FrameUBO)
        };

        VkDescriptorBufferInfo scene_obj_info = {
            .buffer = scene_obj_buffers_[i].buffer,
            .offset = 0,
            .range = sizeof(SceneObjectSBO) * MAX_SCENE_OBJECTS
        };

        VkDescriptorBufferInfo scene_light_info = {
            .buffer = scene_light_buffers_[i].buffer,
            .offset = 0,
            .range = sizeof(SceneLightSBO) * MAX_SCENE_LIGHTS
        };

        VkDescriptorImageInfo image_info = {
            .sampler = texture_sampler_,
            .imageView = texture_.image_view,
            .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
        };

        auto [layout, set] = gfx::DescriptorBuilder::begin(&descriptor_layout_cache_, &descriptor_allocator_)
            .bind_buffer(
                0,
                &buffer_info,
                VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT
            )
            .bind_image(
                1,
                &image_info,
                VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                VK_SHADER_STAGE_FRAGMENT_BIT
            )
            .build();

        descriptor_sets_.push_back(set);

        auto [scene_data_layout, scene_data_set] = gfx::DescriptorBuilder::begin(&descriptor_layout_cache_, &descriptor_allocator_)
            .bind_buffer(
                0,
                &scene_obj_info,
                VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                VK_SHADER_STAGE_VERTEX_BIT
            )
            .bind_buffer(
                1,
                &scene_light_info,
                VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                VK_SHADER_STAGE_FRAGMENT_BIT
            )
            .build();

        scene_data_descriptor_sets_.push_back(scene_data_set);

        auto [texture_descriptor_layout, texture_descriptor_set] = gfx::DescriptorBuilder::begin(&descriptor_layout_cache_, &descriptor_allocator_)
            .bind_bindless(
                0,
                VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                VK_SHADER_STAGE_ALL,
                50
            )
            .build();

        if (i == 0) {
            descriptor_set_layout_ = layout;
            scene_data_descriptor_set_layout_ = scene_data_layout;
        }
    }
}

void VkContext::create_uniform_buffers() {
    VkDeviceSize buffer_size = sizeof(FrameUBO);
    uniform_buffers_.resize(MAX_FRAMES_IN_FLIGHT);
    uniform_buffer_memories_.resize(MAX_FRAMES_IN_FLIGHT);
    uniform_buffer_mappings_.resize(MAX_FRAMES_IN_FLIGHT);
    scene_obj_buffers_.resize(MAX_FRAMES_IN_FLIGHT);
    scene_obj_buffer_mappings_.resize(MAX_FRAMES_IN_FLIGHT);
    scene_light_buffers_.resize(MAX_FRAMES_IN_FLIGHT);
    scene_light_buffer_mappings_.resize(MAX_FRAMES_IN_FLIGHT);


    for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i) {
        VkBufferCreateInfo b_create_info = {
            .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
            .size = buffer_size,
            .usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT
        };

        VkBufferCreateInfo sb_create_info = {
            .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
            .size = sizeof(SceneObjectSBO) * MAX_SCENE_OBJECTS,
            .usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT
        };

        VmaAllocationCreateInfo alloc_create_info = {
            .flags = 
                VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT |
                VMA_ALLOCATION_CREATE_MAPPED_BIT,
            .usage = VMA_MEMORY_USAGE_AUTO,
        };

        VmaAllocationInfo alloc_info;

        vmaCreateBuffer(allocator_, &b_create_info, &alloc_create_info, &uniform_buffers_[i],
                &uniform_buffer_memories_[i], &alloc_info);
        uniform_buffer_mappings_[i] = alloc_info.pMappedData;

        vmaCreateBuffer(allocator_, &sb_create_info, &alloc_create_info, &scene_obj_buffers_[i].buffer,
                &scene_obj_buffers_[i].allocation, &alloc_info);
        scene_obj_buffer_mappings_[i] = alloc_info.pMappedData;

        vmaCreateBuffer(allocator_, &sb_create_info, &alloc_create_info, &scene_light_buffers_[i].buffer,
                &scene_light_buffers_[i].allocation, &alloc_info);
        scene_light_buffer_mappings_[i] = alloc_info.pMappedData;
    }
}

void VkContext::update_uniform_buffer(uint32_t current_image) {
    static auto start_time = std::chrono::high_resolution_clock::now();
    auto current_time = std::chrono::high_resolution_clock::now();

    float delta = std::chrono::duration<float, std::chrono::seconds::period>(current_time - start_time).count();
    auto render_list = render_graph_.get_render_list();

    FrameUBO ubo{};
    //ubo.model = glm::rotate(glm::mat4(1.0f), delta * glm::radians(90.0f), glm::vec3(0.0f,0.0f,1.0f));
    ubo.view = glm::lookAt(
        glm::vec3(2.0f, 2.0f, 1.0f),
        glm::vec3(0.0f,0.0f,0.0f),
        glm::vec3(0.0f,0.0f,1.0f)
    );
    ubo.proj = glm::perspective(
        glm::radians(45.0f),
        extent_.width / (float) extent_.height,
        0.1f, 10.0f
    );
    ubo.proj[1][1] *= -1;
    ubo.light_pos = glm::vec3(2.0f, 3.0f, 2.0f);
    memcpy(uniform_buffer_mappings_[current_image], &ubo, sizeof(ubo));

    for (size_t i = 0; i < render_list.size(); ++i) {
        auto* cur = render_list[i];

        if (cur->frame_update_counter < MAX_FRAMES_IN_FLIGHT) {
            SceneObjectSBO* scene_obj_sbos = (SceneObjectSBO*)scene_obj_buffer_mappings_[current_image];    
            scene_obj_sbos[i].model_mat = cur->model_mat;
            cur->frame_update_counter++;
        }
    }
}

void VkContext::create_texture_image() {
    texture_ = loadTextureFromFile(this, "assets/suzannetex.png");
}

void VkContext::create_image(
    uint32_t width, uint32_t height,
    VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, VkMemoryPropertyFlags properties,
    VkImage* image, VkDeviceMemory* image_memory
){
    VkImageCreateInfo image_info = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        .flags = 0,
        .imageType = VK_IMAGE_TYPE_2D,
        .format = format,
        .extent = {
            .width = static_cast<uint32_t>(width),
            .height = static_cast<uint32_t>(height),
            .depth = 1
        },
        .mipLevels = 1,
        .arrayLayers = 1,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .tiling = tiling,
        .usage = usage,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
    };
    if (vkCreateImage(device_, &image_info, nullptr, image) != VK_SUCCESS){
        throw std::runtime_error("Failed to create VK image");
    }

    VkMemoryRequirements mem_reqs;
    vkGetImageMemoryRequirements(device_, *image, &mem_reqs);

    VkMemoryAllocateInfo alloc_info = {
        .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .allocationSize = mem_reqs.size,
        .memoryTypeIndex = find_memory_type(mem_reqs.memoryTypeBits, properties)
    };

    if (vkAllocateMemory(device_, &alloc_info, nullptr, image_memory) != VK_SUCCESS) {
        throw std::runtime_error("Failed to allocate VK image memory");
    }

    vkBindImageMemory(device_, *image, *image_memory, 0);
}

VkCommandBuffer VkContext::begin_single_time_commands() {
    VkCommandBufferAllocateInfo alloc_info = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .commandPool = command_pool_,
        .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandBufferCount = 1
    };

    VkCommandBuffer buffer;
    vkAllocateCommandBuffers(device_, &alloc_info, &buffer);

    VkCommandBufferBeginInfo begin_info = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT
    };

    vkBeginCommandBuffer(buffer, &begin_info);

    return buffer;
}

void VkContext::end_single_time_commands(VkCommandBuffer buffer){
    vkEndCommandBuffer(buffer);

    VkSubmitInfo submit_info = {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .commandBufferCount = 1,
        .pCommandBuffers = &buffer
    };

    vkQueueSubmit(graphics_queue_, 1, &submit_info, VK_NULL_HANDLE);
    vkQueueWaitIdle(graphics_queue_);

    vkFreeCommandBuffers(device_, command_pool_, 1, &buffer);
}


void VkContext::transition_image_layout(VkImage image, VkFormat format,
    VkImageLayout old_layout, VkImageLayout new_layout
){
    VkCommandBuffer com_buf = begin_single_time_commands();

    VkImageMemoryBarrier barrier = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .srcAccessMask = 0,
        .dstAccessMask = 0,
        .oldLayout = old_layout,
        .newLayout = new_layout,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .image = image,
        .subresourceRange = {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1
        }
    };

    VkPipelineStageFlags src_stage;
    VkPipelineStageFlags dst_stage;

    if (old_layout == VK_IMAGE_LAYOUT_UNDEFINED && new_layout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
        barrier.srcAccessMask = 0;
        barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

        src_stage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        dst_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
    } else if (
        old_layout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL &&
        new_layout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
    ){
        barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

        src_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
        dst_stage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    } else {
        throw std::invalid_argument("Unsupported layout transition");
    }

    vkCmdPipelineBarrier(
        com_buf,
        src_stage, dst_stage,
        0,
        0, nullptr,
        0, nullptr,
        1, &barrier
    );

    end_single_time_commands(com_buf);
}

void VkContext::copy_buffer_to_image(
    VkBuffer buffer, VkImage image, uint32_t width, uint32_t height
){
    VkCommandBuffer com_buf = begin_single_time_commands();
    
    VkBufferImageCopy region = {
        .bufferOffset = 0,
        .bufferRowLength = 0,
        .bufferImageHeight = 0,
        .imageSubresource = {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .mipLevel = 0,
            .baseArrayLayer = 0,
            .layerCount = 1
        },
        .imageOffset = {0, 0, 0},
        .imageExtent = {
            width,
            height,
            1
        }
    };

    vkCmdCopyBufferToImage(
        com_buf,
        buffer,
        image,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        1,
        &region
    );

    end_single_time_commands(com_buf);
}

VkImageView VkContext::create_image_view(VkImage image, VkFormat format, VkImageAspectFlags flags) {
    VkImageViewCreateInfo view_info = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .image = image,
        .viewType = VK_IMAGE_VIEW_TYPE_2D,
        .format = format,
        .subresourceRange = {
            .aspectMask = flags,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1
        }
    };

    VkImageView image_view;
    if (vkCreateImageView(device_, &view_info, nullptr, &image_view) != VK_SUCCESS){
        throw std::runtime_error("Failed to create VK texture image view");
    }
    return image_view;
}

void VkContext::create_texture_sampler() {
    VkPhysicalDeviceProperties properties{};
    vkGetPhysicalDeviceProperties(physical_device_, &properties);

    VkSamplerCreateInfo sampler_info = {
        .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
        .magFilter = VK_FILTER_LINEAR,
        .minFilter = VK_FILTER_LINEAR,
        .mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR,
        .addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .mipLodBias = 0.0f,
        .anisotropyEnable = VK_TRUE,
        .maxAnisotropy = properties.limits.maxSamplerAnisotropy,
        .compareEnable = VK_FALSE,
        .compareOp = VK_COMPARE_OP_ALWAYS,
        .minLod = 0.0f,
        .maxLod = 0.0f,
        .borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK,
        .unnormalizedCoordinates = VK_FALSE,
    };

    if (vkCreateSampler(device_, &sampler_info, nullptr, &texture_sampler_) != VK_SUCCESS) {
        throw(std::runtime_error("Failed to create VK texture sampler"));
    }
}

void VkContext::create_depth_resources() {
    VkFormat depth_format = find_depth_format();
    create_image(extent_.width, extent_.height, depth_format,
        VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &depth_image_, &depth_image_memory_);
    depth_image_view_ = create_image_view(depth_image_, depth_format, VK_IMAGE_ASPECT_DEPTH_BIT);
}

VkFormat VkContext::find_supported_format(
    const std::vector<VkFormat>& candidates,
    VkImageTiling tiling,
    VkFormatFeatureFlags features
){
    for (VkFormat format : candidates) {
        VkFormatProperties props;
        vkGetPhysicalDeviceFormatProperties(physical_device_, format, &props);

        if (tiling == VK_IMAGE_TILING_LINEAR && 
        (props.linearTilingFeatures & features) == features) {
            return format;
        } else if (
            tiling == VK_IMAGE_TILING_OPTIMAL &&
            (props.optimalTilingFeatures & features) == features
        ) {
            return format;
        }
    }

    throw std::runtime_error("Failed to find supported format!");
}

VkFormat VkContext::find_depth_format() {
    return find_supported_format(
        {VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT},
        VK_IMAGE_TILING_OPTIMAL,
        VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
    );
}

bool VkContext::has_stencil_component(VkFormat format) {
    return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
}

void VkContext::create_allocator(){
    VmaVulkanFunctions vulkan_functions = { 
        .vkGetInstanceProcAddr = &vkGetInstanceProcAddr,
        .vkGetDeviceProcAddr = &vkGetDeviceProcAddr
    };

    VmaAllocatorCreateInfo allocator_create_info = {
        .physicalDevice = physical_device_,
        .device = device_,
        .pVulkanFunctions = &vulkan_functions,
        .instance = instance_,
        .vulkanApiVersion = VK_API_VERSION_1_0,
    };

    vmaCreateAllocator(&allocator_create_info, &allocator_);
}

void VkContext::init_imgui() {
    VkDescriptorPoolSize pool_sizes[] =
	{
		{ VK_DESCRIPTOR_TYPE_SAMPLER, 1000 },
		{ VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1000 },
		{ VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, 1000 },
		{ VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1000 },
		{ VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER, 1000 },
		{ VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER, 1000 },
		{ VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1000 },
		{ VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1000 },
		{ VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, 1000 },
		{ VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC, 1000 },
		{ VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, 1000 }
	};

    VkDescriptorPoolCreateInfo pool_info = {};
	pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	pool_info.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
	pool_info.maxSets = 1000;
	pool_info.poolSizeCount = std::size(pool_sizes);
	pool_info.pPoolSizes = pool_sizes;

	VkDescriptorPool imgui_pool;
	VK_CALL(vkCreateDescriptorPool(device_, &pool_info, nullptr, &imgui_pool));
    
    ImGui::CreateContext();

    ImGui_ImplSDL2_InitForVulkan(window_);

    ImGui_ImplVulkan_InitInfo init_info = {};
	init_info.Instance = instance_;
	init_info.PhysicalDevice = physical_device_;
	init_info.Device = device_;
	init_info.Queue = graphics_queue_;
	init_info.DescriptorPool = imgui_pool;
	init_info.MinImageCount = 3;
	init_info.ImageCount = 3;
	init_info.MSAASamples = VK_SAMPLE_COUNT_1_BIT;

    ImGui_ImplVulkan_Init(&init_info, render_pass_);

    VkCommandBuffer cmd_buf = begin_single_time_commands();
    ImGui_ImplVulkan_CreateFontsTexture(cmd_buf);
    end_single_time_commands(cmd_buf);

    ImGui_ImplVulkan_DestroyFontUploadObjects();
}

VkPhysicalDevice* VkContext::get_physical_device() {
    return &physical_device_;
}

RenderGraph* VkContext::get_render_graph() {
    return &render_graph_;
}

}