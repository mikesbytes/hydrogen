#include "hydrogen/graphics/vk/pipeline.hpp"

#include "hydrogen/graphics/vk-context.hpp"

namespace hgn::gfx::vk {

Pipeline::Pipeline(VkContext* context, VkPipeline pipeline, VkPipelineLayout pipeline_layout) :
    context_(context),
    pipeline_(pipeline),
    pipeline_layout_(pipeline_layout),
    initialized_(true)
{}

Pipeline::Pipeline(Pipeline&& rhs) {
    *this = std::move(rhs);
}

Pipeline::~Pipeline() {
    destroy_if_initialized();
}

Pipeline& Pipeline::operator=(Pipeline&& rhs) {
    destroy_if_initialized();

    context_ = rhs.context_;
    pipeline_ = rhs.pipeline_;
    pipeline_layout_ = rhs.pipeline_layout_;
    initialized_ = rhs.initialized_;

    rhs.context_ = nullptr;
    rhs.pipeline_ = VK_NULL_HANDLE;
    rhs.pipeline_layout_ = VK_NULL_HANDLE;
    rhs.initialized_ = false;

    return *this;
}

VkPipeline* Pipeline::get_pipeline() {
    return &pipeline_;
}

void Pipeline::destroy_if_initialized() {
    if (!initialized_) return;

    auto device = *(context_->get_device());

    vkDestroyPipeline(device, pipeline_, nullptr);
    vkDestroyPipelineLayout(device, pipeline_layout_, nullptr);
}

}