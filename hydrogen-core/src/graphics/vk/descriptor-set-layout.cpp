#include "hydrogen/graphics/vk/descriptor-set-layout.hpp"

#include "hydrogen/graphics/vk-context.hpp"

namespace hgn::gfx::vk {

DescriptorSetLayout::DescriptorSetLayout(VkContext* context, VkDescriptorSetLayout layout) :
    context_(context),
    descriptor_set_layout_(layout)
{}

DescriptorSetLayout::DescriptorSetLayout(DescriptorSetLayout&& rhs) {
    *this = std::move(rhs);
}

DescriptorSetLayout::~DescriptorSetLayout() {
    destroy_if_initialized();
}

DescriptorSetLayout& DescriptorSetLayout::operator=(DescriptorSetLayout&& rhs) {
    destroy_if_initialized();

    context_ = rhs.context_;
    descriptor_set_layout_ = rhs.descriptor_set_layout_;

    rhs.context_ = nullptr;
    rhs.descriptor_set_layout_ = VK_NULL_HANDLE;

    return *this;
}

VkDescriptorSetLayout* DescriptorSetLayout::get_raw_descriptor_set_layout() {
    return &descriptor_set_layout_;
}

void DescriptorSetLayout::destroy_if_initialized() {
    if (context_ != nullptr && descriptor_set_layout_ != VK_NULL_HANDLE) {
        vkDestroyDescriptorSetLayout(*(context_->get_device()), descriptor_set_layout_, nullptr);
    }
}

}