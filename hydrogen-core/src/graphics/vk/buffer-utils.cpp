#include "hydrogen/graphics/vk/buffer-utils.hpp"

#include "hydrogen/graphics/vk-context.hpp"

namespace hgn::gfx::vk {
void buffer_copy(VkContext* context,
                 Buffer* src, size_t src_offset,
                 Buffer* dst, size_t dst_offset,
                 size_t size
){
    context->immediate_submit([&](VkCommandBuffer cmd){
        VkBufferCopy copy_region = {
            .srcOffset = src_offset,
            .dstOffset = dst_offset,
            .size = size
        };

        vkCmdCopyBuffer(cmd, *(src->get_buffer()), *(dst->get_buffer()), 1, &copy_region);
    });
}

}