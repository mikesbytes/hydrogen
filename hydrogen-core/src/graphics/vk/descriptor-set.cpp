#include "hydrogen/graphics/vk/descriptor-set.hpp"

#include "hydrogen/graphics/vk-context.hpp"

namespace hgn::gfx::vk {

DescriptorSet::DescriptorSet(VkContext* context, std::shared_ptr<DescriptorSetLayout> descriptor_set_layout, VkDescriptorSet descriptor_set) :
    context_(context),
    descriptor_set_layout_(descriptor_set_layout),
    descriptor_set_(descriptor_set)
{}

DescriptorSet::DescriptorSet(DescriptorSet&& rhs) {

}

DescriptorSet::~DescriptorSet() {

}

DescriptorSet& DescriptorSet::operator=(DescriptorSet&& rhs) {
    context_ = rhs.context_;
    descriptor_set_layout_ = rhs.descriptor_set_layout_;
    descriptor_set_ = rhs.descriptor_set_;

    rhs.context_ = nullptr;
    rhs.descriptor_set_layout_ = nullptr;
    rhs.descriptor_set_ = VK_NULL_HANDLE;
}

void DescriptorSet::destroy_if_initialized() {
    if (context_ == nullptr || descriptor_set_layout_ == VK_NULL_HANDLE) return;

    auto device = *(context_->get_device());
    //vkFreeDescriptorSets(device, pool, 1, &descriptor_set_)
}

}