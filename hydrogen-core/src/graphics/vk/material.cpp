#include "hydrogen/graphics/vk/material.hpp"

#include "hydrogen/graphics/vk/pipeline.hpp"

namespace hgn::gfx::vk {


Material::Material(std::shared_ptr<Pipeline> pipeline) :
    pipeline_(pipeline)
{}

}