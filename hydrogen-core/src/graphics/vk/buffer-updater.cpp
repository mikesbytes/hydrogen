#include "hydrogen/graphics/vk/buffer-updater.hpp"
#include "hydrogen/graphics/vk-context.hpp"

namespace hgn::gfx::vk {


BufferUpdater::BufferUpdater(VkContext* context, size_t max_frames, size_t update_buffer_size):
    context_(context),
    max_frames_(max_frames),
    partial_update_indexes_(max_frames, 0)
{
    partial_update_buffers_.reserve(max_frames);
    for (size_t i = 0; i < max_frames; ++i) {
        auto buffer = Buffer::make_mapped_buffer(
            context,
            update_buffer_size,
            VK_BUFFER_USAGE_TRANSFER_SRC_BIT
        );
        partial_update_buffers_.push_back(std::move(buffer));
    }
}

void BufferUpdater::submit_update(
    void* src, size_t size,
    std::weak_ptr<Buffer> dst, size_t dst_offset,
    size_t frame
) {
    uint8_t* staging_data = (uint8_t*)partial_update_buffers_[frame].get_mapping();
    size_t staging_index = partial_update_indexes_[frame];

    // TODO: Add bounds checking

    memcpy(staging_data + staging_index, src, size);

    PartialBufferUpdate update = {
        .dst_buffer = dst,
        .src_offset = staging_index,
        .dst_offset = dst_offset,
        .size = size
    };

    partial_updates_[frame].push_back(update);

    partial_update_indexes_[frame] += size;
}

void BufferUpdater::process_updates() {
    size_t frame = context_->get_current_frame();
    Buffer& update_src = partial_update_buffers_[frame];
    auto& update_list = partial_updates_[frame];

    context_->frame_submit([&](VkCommandBuffer cmd){
        for (auto& update : update_list) {
            auto dst_shr = update.dst_buffer.lock();
            if (!dst_shr) continue;

            VkBufferCopy copy_region = {
                .srcOffset = update.src_offset,
                .dstOffset = update.dst_offset,
                .size = update.size
            };

            vkCmdCopyBuffer(cmd, *(update_src.get_buffer()), *(dst_shr->get_buffer()), 1, &copy_region);
        }

        VkMemoryBarrier memory_barrier = {
            .sType = VK_STRUCTURE_TYPE_MEMORY_BARRIER,
            .srcAccessMask = 0,
            .dstAccessMask = VK_ACCESS_SHADER_READ_BIT
        };

        vkCmdPipelineBarrier(
            cmd,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            VK_PIPELINE_STAGE_VERTEX_SHADER_BIT | VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
            0,
            1,
            &memory_barrier,
            0,
            nullptr,
            0,
            nullptr
        );
    });

    update_list.clear();
    partial_update_indexes_[frame] = 0;
}
}