#include "hydrogen/graphics/vk/buffer.hpp"

#include "hydrogen/graphics/vk-context.hpp"

namespace hgn::gfx::vk {

Buffer::Buffer() :
    context_(nullptr),
    buffer_(VK_NULL_HANDLE)
{}

Buffer::Buffer(VkContext* context, VkBuffer buffer, VmaAllocation allocation, size_t size, void* mapping) :
    context_(context),
    buffer_(buffer),
    allocation_(allocation),
    mapping_(mapping),
    size_(size)
{}

Buffer::Buffer(Buffer&& rhs) {
    *this = std::move(rhs);
}

Buffer::~Buffer() {
    destroy_if_initialized();
}

Buffer& Buffer::operator=(Buffer&& rhs) {
    destroy_if_initialized();

    context_ = rhs.context_;
    buffer_ = rhs.buffer_;
    allocation_ = rhs.allocation_;
    mapping_ = rhs.mapping_;
    size_ = rhs.size_;

    rhs.context_ = nullptr;
    rhs.buffer_ = VK_NULL_HANDLE;
    rhs.allocation_ = VmaAllocation {};
    rhs.mapping_ = nullptr;
    rhs.size_ = 0;

    return *this;
}

VkBuffer* Buffer::get_buffer() {
    return &buffer_;
}

void* Buffer::get_mapping() {
    return mapping_;
}

void Buffer::destroy_if_initialized() {
    if (context_ != nullptr && buffer_ != VK_NULL_HANDLE) {
        vmaDestroyBuffer(*(context_->get_allocator()), buffer_, allocation_);
    }
}

void* Buffer::map() {
    if (mapping_ != nullptr) return mapping_;

    vmaMapMemory(*(context_->get_allocator()), allocation_, &mapping_);
    return mapping_;
}

void Buffer::unmap() {
    vmaUnmapMemory(*(context_->get_allocator()), allocation_);
}

void Buffer::flush(size_t offset, size_t size) {
    if (size == 0) size = size_;
    vmaFlushAllocation(*(context_->get_allocator()), allocation_, offset, size);
}

void Buffer::invalidate(size_t offset, size_t size) {
    if (size == 0) size = size_;
    vmaInvalidateAllocation(*(context_->get_allocator()), allocation_, offset, size);
}

Buffer Buffer::make_buffer(VkContext* context, size_t size, VkBufferUsageFlags usage) {
    VkBuffer buffer;
    VmaAllocation allocation;

    VkBufferCreateInfo buffer_info = {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = size,
        .usage = usage,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE
    };

    VmaAllocationCreateInfo alloc_info = {
        .usage = VMA_MEMORY_USAGE_AUTO
    };

    VK_CALL(vmaCreateBuffer(*(context->get_allocator()), &buffer_info, &alloc_info, &buffer, &allocation, nullptr));

    return Buffer(context, buffer, allocation, size);
}

Buffer Buffer::make_mapped_buffer(VkContext* context, size_t size, VkBufferUsageFlags usage) {
    VkBuffer buffer;
    VmaAllocation allocation;

    VkBufferCreateInfo buffer_info = {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = size,
        .usage = usage,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE
    };

    VmaAllocationCreateInfo alloc_info = {
        .flags = VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT | VMA_ALLOCATION_CREATE_MAPPED_BIT,
        .usage = VMA_MEMORY_USAGE_AUTO,
    };

    VmaAllocationInfo alloc_info_ret;
    
    VK_CALL(vmaCreateBuffer(*(context->get_allocator()), &buffer_info, &alloc_info, &buffer, &allocation, &alloc_info_ret));

    return Buffer(context, buffer, allocation, size, alloc_info_ret.pMappedData);
}

}