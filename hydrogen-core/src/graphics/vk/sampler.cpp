#include "hydrogen/graphics/vk/sampler.hpp"

#include "hydrogen/graphics/vk-context.hpp"

namespace hgn::gfx::vk {

Sampler::Sampler() :
    context_(nullptr),
    sampler_(VK_NULL_HANDLE)
{}

Sampler::Sampler(Sampler&& rhs) {
    *this = std::move(rhs);
}

Sampler::~Sampler() {
    if (context_ != nullptr && sampler_ != VK_NULL_HANDLE) {
        vkDestroySampler(*(context_->get_device()), sampler_, nullptr);
    }
}

Sampler& Sampler::operator=(Sampler&& rhs) {
    context_ = rhs.context_;
    sampler_ = rhs.sampler_;
    rhs.context_ = nullptr;
    rhs.sampler_ = VK_NULL_HANDLE;
    return *this;
}

VkSampler* Sampler::sampler() {
    return &sampler_;
}

Sampler Sampler::create_default_sampler(VkContext* context) {
    Sampler sampler;
    VkPhysicalDeviceProperties properties{};
    vkGetPhysicalDeviceProperties(*(context->get_physical_device()), &properties);

    VkSamplerCreateInfo sampler_info = {
        .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
        .magFilter = VK_FILTER_LINEAR,
        .minFilter = VK_FILTER_LINEAR,
        .mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR,
        .addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .mipLodBias = 0.0f,
        .anisotropyEnable = VK_TRUE,
        .maxAnisotropy = properties.limits.maxSamplerAnisotropy,
        .compareEnable = VK_FALSE,
        .compareOp = VK_COMPARE_OP_ALWAYS,
        .minLod = 0.0f,
        .maxLod = 0.0f,
        .borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK,
        .unnormalizedCoordinates = VK_FALSE,
    };

    VK_CALL(vkCreateSampler(*(context->get_device()), &sampler_info, nullptr, &sampler.sampler_));

    return sampler;

}

}