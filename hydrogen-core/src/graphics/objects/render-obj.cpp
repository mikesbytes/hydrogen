#include "hydrogen/graphics/objects/render-obj.hpp"

#include <spdlog/spdlog.h>
#include <glm/gtx/quaternion.hpp>

#include "hydrogen/graphics/render-graph.hpp"

namespace hgn::gfx::obj {

RenderObject::RenderObject() :
    position_(0.0f,0.0f,0.0f),
    scale_(1.0f,1.0f,1.0f),
    rotation_(1.0f, 0.0f, 0.0f, 0.0f),
    render_item_(nullptr)
{
    rebuild_model_mat();
}

RenderObject::RenderObject(hgn::gfx::RenderItem* render_item) :
    position_(0.0f,0.0f,0.0f),
    scale_(1.0f,1.0f,1.0f),
    rotation_(1.0f, 0.0f, 0.0f, 0.0f),
    render_item_(render_item)
{
    rebuild_model_mat();
}

RenderObject::~RenderObject() {
    render_item_->mesh.destroy_if_initialized();
    render_item_->free = true;
}

void RenderObject::set_render_item(hgn::gfx::RenderItem* render_item) {
    if (render_item_ != nullptr) {
        render_item_->mesh.destroy_if_initialized();
        render_item_->free = true;
    }

    render_item_ = render_item;
    rebuild_model_mat();
}

void RenderObject::set_position(glm::vec3 position, bool no_rebuild) {
    position_ = position;
    if (!no_rebuild) {
        rebuild_model_mat();
    }
}

glm::vec3 RenderObject::get_position() const {
    return position_;
}

void RenderObject::set_scale(glm::vec3 scale, bool no_rebuild) {
    scale_ = scale;
    if (!no_rebuild) {
        rebuild_model_mat();
    }
}

glm::vec3 RenderObject::get_scale() {
    return scale_;
}

void RenderObject::set_rotation(glm::quat rotation, bool no_rebuild) {
    rotation_ = rotation;
    if (!no_rebuild) {
        rebuild_model_mat();
    }
}

void RenderObject::set_rotation(glm::vec3 euler_rotation, bool no_rebuild) {
    rotation_ = glm::quat(euler_rotation);
    if (!no_rebuild) {
        rebuild_model_mat();
    }
}

glm::quat RenderObject::get_rotation() const {
    return rotation_;
}

void RenderObject::rebuild_model_mat() {
    if (render_item_ == nullptr) {
        return;
    }

    glm::mat4 model_mat = 
        glm::translate(glm::mat4(1.0f), position_) *
        glm::mat4_cast(rotation_) *
        glm::scale(glm::mat4(1.0f), scale_);

    render_item_->model_mat = model_mat;
    render_item_->frame_update_counter = 0;
}

}