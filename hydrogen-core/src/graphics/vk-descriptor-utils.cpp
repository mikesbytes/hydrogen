#include "hydrogen/graphics/vk-descriptor-utils.hpp"

#include <spdlog/spdlog.h>

namespace hgn::gfx {

// DescriptorAllocator impl

DescriptorAllocator::DescriptorAllocator() :
    current_pool_(VK_NULL_HANDLE)
{}

void DescriptorAllocator::init(VkDevice device) {
    device_ = device;
}

void DescriptorAllocator::cleanup() {
    for (auto p : free_pools_) {
        vkDestroyDescriptorPool(device_, p, nullptr);
    }
    for (auto p : used_pools_) {
        vkDestroyDescriptorPool(device_, p, nullptr);
    }
}

VkDescriptorPool DescriptorAllocator::grab_pool() {
    if (free_pools_.size() > 0) {
        VkDescriptorPool pool = free_pools_.back();
        free_pools_.pop_back();
        return pool;
    } else {
        return createPool(device_, descriptor_sizes_, 1000, VK_DESCRIPTOR_POOL_CREATE_UPDATE_AFTER_BIND_BIT);
    }
}

VkDescriptorSet DescriptorAllocator::allocate(VkDescriptorSetLayout layout, std::optional<uint32_t> binding_count) {
    VkDescriptorSet set;
    
    if (current_pool_ == VK_NULL_HANDLE) {
        spdlog::debug("Grabbing pool");
        current_pool_ = grab_pool();
        used_pools_.push_back(current_pool_);
    }


    uint32_t max_binding;
    VkDescriptorSetVariableDescriptorCountAllocateInfoEXT count_info = {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_VARIABLE_DESCRIPTOR_COUNT_ALLOCATE_INFO_EXT,
    };


    VkDescriptorSetAllocateInfo alloc_info = {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
        .pNext = nullptr,
        .descriptorPool = current_pool_,
        .descriptorSetCount = 1,
        .pSetLayouts = &layout,
    };

    if (binding_count.has_value()) {
        max_binding = binding_count.value() - 1;
        count_info.descriptorSetCount = 1;
        count_info.pDescriptorCounts = &max_binding;
        alloc_info.pNext = &count_info;
    }

    VkResult alloc_result = vkAllocateDescriptorSets(device_, &alloc_info, &set);

    switch (alloc_result) {
    case VK_SUCCESS:
        return set;
    case VK_ERROR_FRAGMENTED_POOL:
    case VK_ERROR_OUT_OF_POOL_MEMORY:
        break;
    default:
        throw std::runtime_error("Failed to allocate VK descritor set");
        return VK_NULL_HANDLE;
    }

    current_pool_ = grab_pool();
    used_pools_.push_back(current_pool_);

    VK_CALL(vkAllocateDescriptorSets(device_, &alloc_info, &set));
    return set;
}

void DescriptorAllocator::reset_pools() {
    for (auto p : used_pools_) {
        vkResetDescriptorPool(device_, p, 0);
        free_pools_.push_back(p);
    }

    used_pools_.clear();

    current_pool_ = VK_NULL_HANDLE;
}

// DescriptorLayoutCache impl

bool DescriptorLayoutCache::DescriptorLayoutInfo::operator==(const DescriptorLayoutInfo& other) const {
    if (other.bindings.size() != bindings.size()) return false;

    for (int i = 0; i < bindings.size(); ++i) {
        if (other.bindings[i].binding != bindings[i].binding) return false;
        if (other.bindings[i].descriptorType != bindings[i].descriptorType) return false;
        if (other.bindings[i].descriptorCount != bindings[i].descriptorCount) return false;
        if (other.bindings[i].stageFlags!= bindings[i].stageFlags) return false;
    }

    return true;
}

size_t DescriptorLayoutCache::DescriptorLayoutInfo::hash() const {
    size_t result = std::hash<size_t>()(bindings.size());

    for (const VkDescriptorSetLayoutBinding& b : bindings) {
        size_t binding_hash = b.binding | b.descriptorType << 8 | b.descriptorCount << 16 | b.stageFlags << 24;

        result ^= std::hash<size_t>()(binding_hash);
    }

    return result;
}
void DescriptorLayoutCache::init(VkDevice device) {
    device_ = device;
}

void DescriptorLayoutCache::cleanup() {
    for (auto pair : layout_cache_) {
        vkDestroyDescriptorSetLayout(device_, pair.second, nullptr);
    }
}

VkDescriptorSetLayout DescriptorLayoutCache::create_descriptor_layout(VkDescriptorSetLayoutCreateInfo* info) {
    DescriptorLayoutInfo layout_info;
    layout_info.bindings.reserve(info->bindingCount);
    bool is_sorted = true;
    int last_binding = -1;

    for (int i = 0; i < info->bindingCount; ++i) {
        layout_info.bindings.push_back(info->pBindings[i]);
        
        if (info->pBindings[i].binding > last_binding) {
            last_binding = info->pBindings[i].binding;
        } else {
            is_sorted = false;
        }
    }

    if (!is_sorted) {
        std::sort(
            layout_info.bindings.begin(),
            layout_info.bindings.end(),
            [](VkDescriptorSetLayoutBinding& a, VkDescriptorSetLayoutBinding& b) {
                return a.binding < b.binding;
            }
        );
    }

    auto it = layout_cache_.find(layout_info);
    if (it != layout_cache_.end()) {
        return (*it).second;
    }

    VkDescriptorSetLayout layout;
    vkCreateDescriptorSetLayout(device_, info, nullptr, &layout);

    layout_cache_[layout_info] = layout;
    return layout;
}

// DescriptorBuilder impl


DescriptorBuilder DescriptorBuilder::begin(
    DescriptorLayoutCache* cache,
    DescriptorAllocator* allocator
) {
    DescriptorBuilder builder;

    builder.cache_ = cache;
    builder.allocator_ = allocator;
    builder.has_bindless_bind_ = false;
    builder.bindless_max_resource_count_ = 0;
    return builder;
}

DescriptorBuilder& DescriptorBuilder::bind_buffer(
    uint32_t binding,
    VkDescriptorBufferInfo* buffer_info,
    VkDescriptorType type,
    VkShaderStageFlags flags
) {
    VkDescriptorSetLayoutBinding new_binding = {
        .binding = binding,
        .descriptorType = type,
        .descriptorCount = 1,
        .stageFlags = flags,
        .pImmutableSamplers = nullptr,
    };

    bindings_.push_back(new_binding);

    VkWriteDescriptorSet new_write = {
        .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
        .pNext = nullptr,
        .dstBinding = binding,
        .descriptorCount = 1,
        .descriptorType = type,
        .pBufferInfo = buffer_info,
    };

    writes_.push_back(new_write);

    return *this;
}

DescriptorBuilder& DescriptorBuilder::bind_image(
    uint32_t binding,
    VkDescriptorImageInfo* image_info,
    VkDescriptorType type,
    VkShaderStageFlags flags
) {
    VkDescriptorSetLayoutBinding new_binding = {
        .binding = binding,
        .descriptorType = type,
        .descriptorCount = 1,
        .stageFlags = flags,
        .pImmutableSamplers = nullptr,
    };

    bindings_.push_back(new_binding);

    VkWriteDescriptorSet new_write = {
        .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
        .pNext = nullptr,
        .dstBinding = binding,
        .descriptorCount = 1,
        .descriptorType = type,
        .pImageInfo = image_info,
    };

    writes_.push_back(new_write);

    return *this;
}

DescriptorBuilder& DescriptorBuilder::bind_bindless(
        uint32_t binding,
        VkDescriptorType type,
        VkShaderStageFlags flags,
        uint32_t max_resources
) {
    VkDescriptorSetLayoutBinding new_binding = {
        .binding = binding,
        .descriptorType = type,
        .descriptorCount = max_resources,
        .stageFlags = flags,
        .pImmutableSamplers = nullptr,
    };
    spdlog::debug("max resource: {}", new_binding.descriptorCount);

    bindings_.push_back(new_binding);

    has_bindless_bind_ = true;
    bindless_max_resource_count_ = max_resources;

    return *this;
}
    
std::pair<VkDescriptorSetLayout, VkDescriptorSet> DescriptorBuilder::build() {
    VkDescriptorSetLayout layout;
    VkDescriptorSet set;

    if (has_bindless_bind_) {
        VkDescriptorBindingFlags bindless_flags = VK_DESCRIPTOR_BINDING_PARTIALLY_BOUND_BIT_EXT |
                                                  VK_DESCRIPTOR_BINDING_VARIABLE_DESCRIPTOR_COUNT_BIT_EXT |
                                                  VK_DESCRIPTOR_BINDING_UPDATE_AFTER_BIND_BIT_EXT;

        VkDescriptorSetLayoutBindingFlagsCreateInfoEXT bindless_info = {
            .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO_EXT,
            .bindingCount = (uint32_t)bindings_.size(),
            .pBindingFlags = &bindless_flags 
        };
        
        VkDescriptorSetLayoutCreateInfo layout_info = {
            .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
            .pNext = &bindless_info,
            .flags = VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT_EXT,
            .bindingCount = (uint32_t)bindings_.size(),
            .pBindings = bindings_.data(),
        };
        

        layout = cache_->create_descriptor_layout(&layout_info);
        set = allocator_->allocate(layout, bindless_max_resource_count_);
    } else {
        VkDescriptorSetLayoutCreateInfo layout_info = {
            .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
            .pNext = nullptr,
            .bindingCount = (uint32_t)bindings_.size(),
            .pBindings = bindings_.data(),
        };

        layout = cache_->create_descriptor_layout(&layout_info);
        set = allocator_->allocate(layout);
    }


    for (VkWriteDescriptorSet& w : writes_) {
        w.dstSet = set;
    }


    vkUpdateDescriptorSets(allocator_->device_, writes_.size(), writes_.data(), 0, nullptr);

    return { layout, set };
}

// createPool impl

VkDescriptorPool createPool(
    VkDevice device,
    const DescriptorAllocator::PoolSizes& pool_sizes,
    int count,
    VkDescriptorPoolCreateFlags flags
) {
    std::vector<VkDescriptorPoolSize> sizes;
    sizes.reserve(pool_sizes.sizes.size());

    for (auto sz : pool_sizes.sizes) {
        sizes.push_back({sz.first, uint32_t(sz.second * count)});
    }

    VkDescriptorPoolCreateInfo pool_info = {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        .flags = flags,
        .maxSets = (uint32_t)count,
        .poolSizeCount = (uint32_t)sizes.size(),
        .pPoolSizes = sizes.data()
    };

    VkDescriptorPool descriptor_pool;
    vkCreateDescriptorPool(device, &pool_info, nullptr, &descriptor_pool);

    return descriptor_pool;
}

}