#include "hydrogen/graphics/vk-mesh.hpp"

#include <tiny_obj_loader.h>
#include <fmt/format.h>

#include "hydrogen/graphics/vk-context.hpp"

namespace hgn::gfx::vk {

Mesh::Mesh() :
    initialized(false)
{}

Mesh::Mesh(VkContext* context, vk::Buffer&& vertex_buffer, vk::Buffer&& index_buffer,
    size_t vertex_count, size_t index_count
) :
    context_(context),
    vertex_buffer(std::move(vertex_buffer)),
    index_buffer(std::move(index_buffer)),
    initialized(true),
    vertex_count_(vertex_count),
    index_count_(index_count)
{}

Mesh::Mesh(Mesh&& rhs) {
    *this = std::move(rhs);
}

Mesh::~Mesh() {
    destroy_if_initialized();
}

Mesh& Mesh::operator=(Mesh&& rhs) {
    destroy_if_initialized();

    context_ = rhs.context_;
    vertex_buffer = std::move(rhs.vertex_buffer);
    index_buffer = std::move(rhs.index_buffer);
    initialized = rhs.initialized;
    vertex_count_ = rhs.vertex_count_;
    index_count_ = rhs.index_count_;

    rhs.context_ = nullptr;
    rhs.initialized = false;
    rhs.vertex_count_ = 0;
    rhs.index_count_ = 0;

    return *this;
}

size_t Mesh::vertex_count() const {
    return vertex_count_;
}

size_t Mesh::index_count() const {
    return index_count_;
}

void Mesh::destroy_if_initialized() {
    if (initialized) {
        vertex_count_ = 0;
        index_count_ = 0;
        initialized = false;
    }
}

VertexInputDescription Vertex::get_vertex_description(){
    VertexInputDescription description;

    VkVertexInputBindingDescription main_binding = {
        .binding = 0,
        .stride = sizeof(Vertex),
        .inputRate = VK_VERTEX_INPUT_RATE_VERTEX
    };
    description.bindings.push_back(main_binding);

    VkVertexInputAttributeDescription pos_attr = {
        .location = 0,
        .binding = 0,
        .format = VK_FORMAT_R32G32B32_SFLOAT,
        .offset = offsetof(Vertex, position),
    };
    description.attributes.push_back(pos_attr);

    VkVertexInputAttributeDescription norm_attr = {
        .location = 1,
        .binding = 0,
        .format = VK_FORMAT_R32G32B32_SFLOAT,
        .offset = offsetof(Vertex, normal),
    };
    description.attributes.push_back(norm_attr);    

    VkVertexInputAttributeDescription uv_attr = {
        .location = 2,
        .binding = 0,
        .format = VK_FORMAT_R32G32_SFLOAT,
        .offset = offsetof(Vertex, uv),
    };
    description.attributes.push_back(uv_attr);    

    return description;
}

Mesh loadMesh(vk::VkContext* ctx, const std::vector<Vertex>& vertices, const std::vector<uint32_t>& indices) {
    VmaAllocator* allocator = ctx->get_allocator();

    // transfer allocation info
    VmaAllocationCreateInfo tf_alloc_info = {
        .usage = VMA_MEMORY_USAGE_CPU_ONLY
    };
    
    // final allocation info
    VmaAllocationCreateInfo alloc_info = {
        .usage = VMA_MEMORY_USAGE_GPU_ONLY
    };

    // transfer buffers
    vk::Buffer v_tf_buffer = vk::Buffer::make_mapped_buffer(ctx, vertices.size() * sizeof(Vertex), VK_BUFFER_USAGE_TRANSFER_SRC_BIT);
    vk::Buffer i_tf_buffer = vk::Buffer::make_mapped_buffer(ctx, indices.size() * sizeof(uint32_t), VK_BUFFER_USAGE_TRANSFER_SRC_BIT);

    //AllocatedBuffer v_tf_buffer;
    //AllocatedBuffer i_tf_buffer;

    // final buffers
    vk::Buffer v_buffer = vk::Buffer::make_buffer(ctx, vertices.size() * sizeof(Vertex), VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT);
    vk::Buffer i_buffer = vk::Buffer::make_buffer(ctx, indices.size() * sizeof(uint32_t), VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT);

    //AllocatedBuffer v_buffer;
    //AllocatedBuffer i_buffer;
    
    /*
    VkBufferCreateInfo v_tf_buffer_info = {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = vertices.size() * sizeof(Vertex),
        .usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT
    };
    VkBufferCreateInfo i_tf_buffer_info = {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = indices.size() * sizeof(uint32_t),
        .usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT
    };

    VkBufferCreateInfo v_buffer_info = {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = vertices.size() * sizeof(Vertex),
        .usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT
    };
    VkBufferCreateInfo i_buffer_info = {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = indices.size() * sizeof(uint32_t),
        .usage = VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT
    };
    
    VK_CALL(vmaCreateBuffer(
        *allocator, &v_tf_buffer_info, &tf_alloc_info,
        &v_tf_buffer.buffer, &v_tf_buffer.allocation,
        nullptr
    ));
    VK_CALL(vmaCreateBuffer(
        *allocator, &i_tf_buffer_info, &tf_alloc_info,
        &i_tf_buffer.buffer, &i_tf_buffer.allocation,
        nullptr
    ));
    
    VK_CALL(vmaCreateBuffer(
        *allocator, &v_buffer_info, &alloc_info,
        &v_buffer.buffer, &v_buffer.allocation,
        nullptr
    ));
    VK_CALL(vmaCreateBuffer(
        *allocator, &i_buffer_info, &alloc_info,
        &i_buffer.buffer, &i_buffer.allocation,
        nullptr
    ));
    */

    //void* v_data;
    //vmaMapMemory(*allocator, v_tf_buffer.allocation, &v_data);
    memcpy(v_tf_buffer.get_mapping(), vertices.data(), vertices.size() * sizeof(Vertex));
    //vmaUnmapMemory(*allocator, v_tf_buffer.allocation);

    //void* i_data;
    //vmaMapMemory(*allocator, i_tf_buffer.allocation, &i_data);
    memcpy(i_tf_buffer.get_mapping(), indices.data(), indices.size() * sizeof(uint32_t));
    //vmaUnmapMemory(*allocator, i_tf_buffer.allocation);

    ctx->copy_buffer(*(v_tf_buffer.get_buffer()), *(v_buffer.get_buffer()), vertices.size() * sizeof(Vertex));
    ctx->copy_buffer(*(i_tf_buffer.get_buffer()), *(i_buffer.get_buffer()), indices.size() * sizeof(uint32_t));

    //vmaDestroyBuffer(*allocator, v_tf_buffer.buffer, v_tf_buffer.allocation);
    //vmaDestroyBuffer(*allocator, i_tf_buffer.buffer, i_tf_buffer.allocation);

    return Mesh(ctx, std::move(v_buffer), std::move(i_buffer), vertices.size(), indices.size());
}

Mesh loadMeshFromOBJ(vk::VkContext* ctx, const std::string& file_name) {

    tinyobj::ObjReaderConfig reader_config;
    tinyobj::ObjReader reader;

    if (!reader.ParseFromFile(file_name, reader_config)) {
        if (!reader.Error().empty()) {
            throw std::runtime_error(fmt::format("TinyObjReader on file {}:\n>>>{}",file_name, reader.Error()));
        }
        throw std::runtime_error(fmt::format("Error reading OBJ file: {}", file_name));
    }

    auto& attrib = reader.GetAttrib();
    auto& shapes = reader.GetShapes();
    auto& materials = reader.GetMaterials();

    std::vector<Vertex> vertices;
    std::vector<uint32_t> indices;
    uint32_t index_count = 0;

    for (size_t s = 0; s < shapes.size(); ++s) {
        size_t index_offset = 0;
        for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); ++f) {
            int fv = 3;
            for (size_t v = 0; v < fv; ++v) {
                tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];

                //pos
                tinyobj::real_t vx = attrib.vertices[3 * idx.vertex_index + 0];
                tinyobj::real_t vy = attrib.vertices[3 * idx.vertex_index + 1];
                tinyobj::real_t vz = attrib.vertices[3 * idx.vertex_index + 2];

                //norm
                tinyobj::real_t nx = attrib.normals[3 * idx.normal_index + 0];
                tinyobj::real_t ny = attrib.normals[3 * idx.normal_index + 1];
                tinyobj::real_t nz = attrib.normals[3 * idx.normal_index + 2];

                //uv
                tinyobj::real_t tu = attrib.texcoords[2 * idx.texcoord_index + 0];
                tinyobj::real_t tv = attrib.texcoords[2 * idx.texcoord_index + 1];

                Vertex vert = {
                    .position = {vx, vy, vz},
                    .normal = {nx, ny, nz},
                    .uv = {tu, 1.0f - tv}
                };

                vertices.push_back(vert);
                indices.push_back(index_count);                
                ++index_count;
            }
            index_offset += fv;
        }
    }

    return loadMesh(ctx, vertices, indices);
}

}