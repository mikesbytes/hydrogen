#include "hydrogen/graphics/model-loader.hpp"

#include <tiny_gltf.h>
#include <spdlog/spdlog.h>

namespace hgn::gfx {

std::optional<ModelData> looadGLTFModel(const std::string& model_path){
    spdlog::info("Loading model: {}", model_path);

    tinygltf::Model model;
    tinygltf::TinyGLTF loader;
    std::string err;
    std::string warn;

    bool ret = loader.LoadASCIIFromFile(&model, &err, &warn, model_path);

    if (!warn.empty()) {
        spdlog::warn("Model loader warning on {}:\n>>>{}", model_path, warn);
    }

    if (!err.empty()) {
        spdlog::error("Model loader error on {}:\n>>>{}", model_path, err);
    }

    if (!ret) {
        spdlog::error("Model loader failed on {}", model_path);
        return std::nullopt;
    }
}
}