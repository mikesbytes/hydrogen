#include <spdlog/spdlog.h>

#include "hydrogen/graphics/vk-context.hpp"
#include "hydrogen/graphics/vk/sampler.hpp"
#include "hydrogen/graphics/vk-mesh.hpp"
#include "hydrogen/graphics/objects/render-obj.hpp"

#include "taskflow/taskflow.hpp"

int main() {

    tf::Executor executor;
    tf::Taskflow taskflow;

    hgn::gfx::vk::VkContext context;
    bool running = true;

    tf::Task init = taskflow.emplace(
        [&context](){
            spdlog::set_level(spdlog::level::debug);
            context.init_vulkan();

        }
    ).name("init");

    executor.run(taskflow).wait();


    {
        // create texture
        auto texture = hgn::gfx::loadTextureFromFile(&context, "assets/suzannetex.png");

        // create sampler
        auto sampler = hgn::gfx::vk::Sampler::create_default_sampler(&context);


        // add mesh to render graph
        auto* render_item = context.get_render_graph()->add_render_object(
            hgn::gfx::RenderItem {
                .mesh = hgn::gfx::vk::loadMeshFromOBJ(&context, "assets/suzanne.obj"),
                .model_mat = glm::mat4(1.0f)
            }
        );

        hgn::gfx::obj::RenderObject render_obj;
        render_obj.set_render_item(render_item);
        render_obj.set_rotation({
            0.5f * std::numbers::pi,
            0.0f,
            0.5f * std::numbers::pi});
        
        // descriptor stuff
        

        auto start_time = std::chrono::high_resolution_clock::now();

        auto event_handler = [&running](SDL_Event evt) {
            if (evt.type == SDL_QUIT) {
                running = false;
            }
        };

        while (running == true) {
            context.process_events(event_handler);
            context.start_gui_frame();
            context.end_gui_frame();
            context.draw_frame();

            auto current_time = std::chrono::high_resolution_clock::now();

            float delta = std::chrono::duration<float, std::chrono::seconds::period>(current_time - start_time).count();

            render_obj.set_rotation({
                0.5f * std::numbers::pi,
                0.0f,
                1.0f * (2*std::numbers::pi) * delta});

            context.sync_frame_data();
            context.present_frame();
        }

    }
    context.cleanup();
    return 0;
}