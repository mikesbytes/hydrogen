# CMakeLists.txt
cmake_minimum_required(VERSION 3.0)
project(hydrogen)

set(CMAKE_CXX_STANDARD 20)


find_package(unofficial-sqlite3 CONFIG REQUIRED)
find_package(glm CONFIG REQUIRED)
find_package(SDL2 CONFIG REQUIRED)
find_package(Vulkan REQUIRED)
find_package(fmt CONFIG REQUIRED)
find_package(spdlog CONFIG REQUIRED)
find_package(Stb REQUIRED)
find_path(TINYGLTF_INCLUDE_DIRS "tiny_gltf.h")
find_package(unofficial-vulkan-memory-allocator CONFIG REQUIRED)
find_package(tinyobjloader CONFIG REQUIRED)
find_package(imgui CONFIG REQUIRED)
find_package(Taskflow CONFIG REQUIRED)

add_executable(hydrogen-core
    hydrogen-core/src/core/engine-runner.cpp
    hydrogen-core/src/core/task-graph.cpp
    hydrogen-core/src/graphics/camera.cpp
    hydrogen-core/src/graphics/model-loader.cpp
    hydrogen-core/src/graphics/objects/pipeline-obj.cpp
    hydrogen-core/src/graphics/objects/render-obj.cpp
    hydrogen-core/src/graphics/objects/texture-obj.cpp
    hydrogen-core/src/graphics/render-graph.cpp
    hydrogen-core/src/graphics/texture-manager.cpp
    hydrogen-core/src/graphics/vk-context.cpp
    hydrogen-core/src/graphics/vk-descriptor-utils.cpp
    hydrogen-core/src/graphics/vk-mesh.cpp
    hydrogen-core/src/graphics/vk-pipeline-utils.cpp
    hydrogen-core/src/graphics/vk-texture.cpp
    hydrogen-core/src/graphics/vk/sampler.cpp
    hydrogen-core/src/graphics/vk/image.cpp
    hydrogen-core/src/graphics/vk/image-view.cpp
    hydrogen-core/src/graphics/vk/material.cpp
    hydrogen-core/src/graphics/vk/pipeline.cpp
    hydrogen-core/src/graphics/vk/descriptor-set.cpp
    hydrogen-core/src/graphics/vk/descriptor-set-layout.cpp
    hydrogen-core/src/graphics/vk/buffer-updater.cpp
    hydrogen-core/src/graphics/vk/buffer.cpp
    hydrogen-core/src/graphics/vk/buffer-utils.cpp
    hydrogen-core/src/lib-impl/stb-image.cpp
    hydrogen-core/src/lib-impl/tinygltf.cpp
    hydrogen-core/src/lib-impl/vulkan-memory-allocator.cpp
    hydrogen-core/src/main.cpp
    hydrogen-core/src/utils/fileloader.cpp

)


target_include_directories(hydrogen-core PRIVATE hydrogen-core/include ${Stb_INCLUDE_DIR} ${TINYGLTF_INCLUDE_DIRS})
target_link_libraries(hydrogen-core PRIVATE 
    unofficial::sqlite3::sqlite3
    glm::glm SDL2::SDL2
    fmt::fmt
    dl
    Vulkan::Vulkan
    spdlog::spdlog
    unofficial::vulkan-memory-allocator::vulkan-memory-allocator
    pthread
    tinyobjloader::tinyobjloader
    imgui::imgui
    Taskflow::Taskflow)