Formatting rules:

Formatting:

```
GLOBALS_GET_SCREAMING_SNAKE

globalFunctionsGetCamel

ClassesGetPascal

Class::member_functions_get_snake

Class::member_vars_get_trailing_underscore_
```

Namespace goes on top level, contents are not indented

```
namespace foo::bar {

class baz {};

}
```

Open curly braces should almost never be placed on their own line