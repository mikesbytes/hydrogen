#version 460

layout(set = 0, binding = 0) uniform UniformBufferObject{
    mat4 model;
    mat4 view;
    mat4 proj;
    vec3 light_pos;
    vec3 camera_pos;
} ubo;

struct SceneObjSBO {
    mat4 model_mat;
};

layout(std140,set = 1, binding = 0) readonly buffer SceneObjBuffer {
    SceneObjSBO objects[];
} sceneObjBuffer;


layout(location = 0) in vec3 in_position;
layout(location = 1) in vec3 in_norm;
layout(location = 2) in vec2 in_tex_coord;

layout(location = 0) out vec3 frag_color;
layout(location = 1) out vec2 frag_tex_coord;
layout(location = 2) out vec3 out_norm;
layout(location = 3) out vec3 out_frag_pos;

void main() {
    mat4 model_mat = sceneObjBuffer.objects[gl_BaseInstance].model_mat;
    gl_Position = ubo.proj * ubo.view * model_mat * vec4(in_position, 1.0);
    frag_tex_coord = in_tex_coord;
    out_norm = mat3(transpose(inverse(model_mat))) * in_norm;
    out_frag_pos = vec3(model_mat * vec4(in_position, 1.0));
}