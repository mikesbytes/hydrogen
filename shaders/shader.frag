#version 460
layout (location = 0) in vec3 frag_color;
layout (location = 1) in vec2 frag_tex_coord;
layout (location = 2) in vec3 in_norm;
layout (location = 3) in vec3 in_pos;

layout (location = 0) out vec4 outColor;

layout(binding = 0) uniform UniformBufferObject{
    mat4 model;
    mat4 view;
    mat4 proj;
    vec3 light_pos;
    vec3 camera_pos;
} ubo;

layout (binding = 1) uniform sampler2D tex_sampler;

layout(std140, set = 1, binding = 1) readonly buffer SceneLightBuffer {
    mat4 model_mat;
} SceneLightSBO;

void main() {
    vec3 obj_color = vec3(texture(tex_sampler, frag_tex_coord));
    vec3 light_color = vec3(1.0, 1.0, 1.0);
    float ambient_light_strength = 0.2;

    vec3 ambient_val = light_color * ambient_light_strength;

    vec3 norm = normalize(in_norm);
    vec3 light_dir = normalize(ubo.light_pos - in_pos);

    float diffuse_fac = max(dot(norm, light_dir), 0.0);
    vec3 diffuse_val = diffuse_fac * light_color;

    vec3 result = (ambient_val + diffuse_val) * obj_color ;
    outColor = vec4(result, 1.0);
}